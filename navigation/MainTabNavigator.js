import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';

import LoadingScreen from '../screens/LoadingScreen';
import FirstScreen from '../screens/FirstScreen';
import MainScreen from '../screens/MainScreen';
import LoginScreen from '../screens/LoginScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Loading: LoadingScreen,
    First: FirstScreen,
    Main: MainScreen,
    Login: LoginScreen,
  },
  {
    initialRouteName: 'Loading'
  },
  config
);

export default HomeStack;
