import React, { Component } from 'react';
import { StyleSheet, View, Image, ImageBackground, TouchableOpacity, Alert } from 'react-native';
import Swiper from 'react-native-swiper';
import { height, width } from '../tools/Layout';
import firebase from '../tools/firebase';
import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import * as AppleAuthentication from "expo-apple-authentication";
import * as Crypto from "expo-crypto";
import * as SQLite from 'expo-sqlite';
import ExpoMixpanelAnalytics from '@benawad/expo-mixpanel-analytics';

const analytics = new ExpoMixpanelAnalytics('b1baf3361ff286c515c936ea19262112')

const db = SQLite.openDatabase('todok.db');

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export default class FirstScreen extends Component {

    state = {
        overlay: false,
        email: null,
        password: null,
        extraScrollHeight: 0,
        create: true,
    };

    componentDidMount() {
        db.transaction(
            tx => {
                tx.executeSql("drop table if exists users");
                tx.executeSql("drop table if exists lists");
                tx.executeSql("drop table if exists pages");
                tx.executeSql("drop table if exists bubbles");
            });
    }

    async signInWithLoginEmail() {
        if (!validateEmail(this.state.email)) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        if (!this.state.password) {
            Alert.alert('Merci d\'entrer un mot de passe valide');
            return;
        }
        await firebase
            .auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .then((e) => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email, this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        //await AsyncStorage.getItem("alreadyAskTodok").then(async value => {
                        // if (value == null) {
                        // await AsyncStorage.setItem("alreadyAskTodok", 'true');
                        this.props.navigation.navigate('Main');
                        // }
                        //});
                    })
                    .catch((error) => {
                        Alert.alert('Une erreur est survenue')
                    });
            }).catch(error => {
                let errorCode = error.code;
                let errorMessage = error.message;
                if (errorCode === 'auth/weak-password') {
                    Alert.alert('Le mot de passe est incorrect')
                } else {
                    Alert.alert(errorMessage)
                }
            });
    }

    async signInWithEmail() {
        console.log("MAIL")
        if (!validateEmail(this.state.email)) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((e) => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email, this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        //await AsyncStorage.getItem("alreadyAskTodok").then(async value => {
                        // if (value == null) {
                        // await AsyncStorage.setItem("alreadyAskTodok", 'true');

                        //await firebase.firestore().collection('users')
                        //  .doc(user.uid).set({ gender: 'man', birthday: this.state.birthday, job: this.state.job })
                        //.then(() => this.props.navigation.navigate('Ask'));
                        // }
                        //});
                    })
                    .catch((error) => {
                        Alert.alert('Une erreur est survenue')
                    });
            }).catch((error) => {
                if (String(error).includes('Password')) {
                    Alert.alert('Votre mot de passe doit contenir au moins 6 caractères.');
                    return;
                }
                if (String(error).includes('email')) {
                    Alert.alert('Votre email a déjà été utilisée');
                    return;
                }
                Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊');
            });
    }

    async signInWithApple() {
        const csrf = Math.random().toString(36).substring(2, 15);
        const nonce = Math.random().toString(36).substring(2, 10);
        const hashedNonce = await Crypto.digestStringAsync(Crypto.CryptoDigestAlgorithm.SHA256, nonce);
        try {
            const data = await AppleAuthentication.signInAsync({
                requestedScopes: [
                    AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                    AppleAuthentication.AppleAuthenticationScope.EMAIL,
                ],
                state: csrf,
                nonce: hashedNonce
            });
            const {
                identityToken,
            } = data;
            if (identityToken) {
                const provider = new firebase.auth.OAuthProvider("apple.com");
                const credential = provider.credential({
                    idToken: data.identityToken,
                    rawNonce: nonce,
                });
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        //await AsyncStorage.getItem("alreadyAskTodok").then(async value => {
                        //  if (value == null) {
                        //   await AsyncStorage.setItem("alreadyAskTodok", 'true');
                        const user = firebase.auth().currentUser;

                        if (!user.uid) {
                            Alert.alert('Merci d\'essayer de vous reconnecter');
                            return;
                        }
                        const id1 = Math.random().toString(36).substring(3);
                        const id2 = Math.random().toString(36).substring(3);
                        const ref = 'local' + user.uid + 'Todok' + id1 + id2;
                        await firebase.firestore().collection('users')
                            .doc(user.uid).get().then(async (docRef) => {
                                if (docRef.exists)
                                    await firebase.firestore().collection('users')
                                        .doc(user.uid).update({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                        .then(() => {
                                            this.props.navigation.navigate('Main', { session: ref })
                                        });
                                else
                                    await firebase.firestore().collection('users')
                                        .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                        .then(() => {
                                            analytics.track("Signed up", { "None": "None" });
                                            this.props.navigation.navigate('Main', { session: ref })
                                        });
                            })
                        //  }
                        //  console.log("LA" + value)
                        //}).catch((e) => console.log(e));
                    }).catch((e) => console.log(e));
            }
        } catch (e) {
            console.log(e);
        }
    }

    signInWithGoogle = async () => {
        try {
            const result = await Google.logInAsync({
                androidClientId: '35098064275-00b6tgijedhc2e7lfen6hq3s26jtabp2.apps.googleusercontent.com',
                iosClientId: '35098064275-bf65156jsneeqen71ekf620mtesptgd9.apps.googleusercontent.com',
                scopes: ["profile", "email"]
            });
            if (result.type === "success") {
                const credential = firebase.auth.GoogleAuthProvider.credential(result.idToken);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        //await AsyncStorage.getItem("alreadyAskTodok").then(async value => {
                        //  if (value == null) {
                        //   await AsyncStorage.setItem("alreadyAskTodok", 'true');
                        const user = firebase.auth().currentUser;

                        if (!user.uid) {
                            Alert.alert('Merci d\'essayer de vous reconnecter');
                            return;
                        }
                        await firebase.firestore().collection('users')
                            .doc(user.uid).get().then(async (docRef) => {
                                if (docRef.exists)
                                    await firebase.firestore().collection('users')
                                        .doc(user.uid).update({ gender: "undefined", birthday: "undefined", job: "undefined" })
                                        .then(() => {
                                            this.props.navigation.navigate('Main')
                                        });
                                else
                                    await firebase.firestore().collection('users')
                                        .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined" })
                                        .then(() => {
                                            this.props.navigation.navigate('Main')
                                        });
                            })
                        //  }
                        //  console.log("LA" + value)
                        //}).catch((e) => console.log(e));
                    }).catch((e) => console.log(e));
            } else {
                Alert.alert('Une erreur est survenue, merci de réessayer');
            }
        } catch (e) {
            console.log(e);
        }
    };

    async signInWithFacebook() {
        const appId = '213933436523671';

        await Facebook.initializeAsync(appId);
        const {
            type,
            token,
        } = await Facebook.logInWithReadPermissionsAsync({
            permissions: ['public_profile', 'email'],
        });

        if (type == 'success') {
            await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);  // Set persistent auth state
            const credential = firebase.auth.FacebookAuthProvider.credential(token);
            await firebase.auth().signInWithCredential(credential)  // Sign in with Facebook credential
                .then(async () => {
                    const user = firebase.auth().currentUser;

                    if (!user.uid) {
                        Alert.alert('Merci d\'essayer de vous reconnecter');
                        return;
                    }
                    await firebase.firestore().collection('users')
                        .doc(user.uid).get().then(async (docRef) => {
                            if (docRef.exists)
                                await firebase.firestore().collection('users')
                                    .doc(user.uid).update({ gender: "undefined", birthday: "undefined", job: "undefined" })
                                    .then(() => {
                                        this.props.navigation.navigate('Main')
                                    });
                            else
                                await firebase.firestore().collection('users')
                                    .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined" })
                                    .then(() => {
                                        analytics.track("Signed up", { "None": "None" });
                                        this.props.navigation.navigate('Main')
                                    });
                        })
                })
                .catch((error) => {
                    if (String(error).includes("An account already exists with the same email address but different sign-in credentials."))
                        Alert.alert("Un compte existe déjà avec cette adresse e-mail.")
                    console.log(error)
                });
            // Do something with Facebook profile data
            // OR you have subscribed to auth state change, authStateChange handler will process the profile data
        }
    }

    render() {
        return (
            <Swiper showsButtons={false} showsPagination={false}>
                <View style={styles.slide}>
                    <ImageBackground source={require('../assets/frame1.jpg')} style={{ height: '100%', width: '100%' }} />
                </View>
                <View style={styles.slide}>
                    <ImageBackground source={require('../assets/frame2.jpg')} style={{ height: '100%', width: '100%' }} />
                </View>
                <View style={styles.slide}>
                    <ImageBackground source={require('../assets/frame3.jpg')} style={{ height: '100%', width: '100%' }} />
                </View>
                <View style={styles.slide}>
                    <ImageBackground source={require('../assets/frameback.png')} style={{ height: '100%', width: '100%', alignItems: 'center' }}>
                        <View style={{ flex: 1 }} />
                        <View style={{ flex: 5 }}>
                            <View style={styles.create_with}>
                                <Image source={require('../assets/connexion.png')} resizeMode="contain" style={styles.create_account_design} />
                            </View>
                            <View style={styles.create_with_sec}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login', { analytics })} style={styles.create_bubble}>
                                    <Image source={require('../assets/todoklog.png')} resizeMode="contain" style={styles.bubble_logo} />
                                </TouchableOpacity>
                                {/* <TouchableOpacity onPress={() => this.signInWithApple()} style={styles.create_bubble}>
                                    <Image source={require('../assets/applelog.png')} resizeMode="contain" style={styles.bubble_logo} />
                                </TouchableOpacity> */}
                                {/* <TouchableOpacity onPress={() => this.signInWithApple()} style={styles.create_bubble}>
                  <Image source={require('../assets/apple.png')} resizeMode="contain" style={styles.bubble_logo} />
                  <Text style={styles.bubble_text}>Apple</Text>
                  <Image source={require('../assets/go.png')} resizeMode="contain" style={styles.go_design} />
                </TouchableOpacity> */}
                            </View>
                        </View>
                        <View style={{ flex: 1 }} />
                    </ImageBackground>
                </View>
            </Swiper>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bubble_text: {
        marginTop: 5,
        marginLeft: 15,
        color: '#000',
        fontFamily: 'avenirbook',
        fontSize: 17,
        flexGrow: 1,
    },
    create_with: {
        flex: 2,
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    create_with_sec: {
        flex: 3,
        width: width,
        alignItems: 'center',
    },
    bubble_logo: {
        height: 50,
        width: width * 0.8,
    },
    go_design: {
        height: 15,
        width: 15,
        marginRight: 20,
    },
    create_account_design: {
        height: '100%',
        width: '60%'
    },
    create_design: {
        height: '100%',
        width: '90%'
    },
    create_bubble: {
        height: 50,
        marginVertical: 10,
        width: width * 0.8,
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    wrapper: {
        height: height * 0.6,
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    underlined: {
        textDecorationLine: "underline"
    },
    subpart: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height * /*0.17*/ 0.22,
        paddingHorizontal: 20,
    },
    textlittle: {
        textAlign: 'center',
        fontSize: 11,
        color: '#C4C4C4',
        fontFamily: 'avenirbook'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 15,
        elevation: 15
    }
});

FirstScreen.navigationOptions = {
    header: null,
    gesturesEnabled: false,
};