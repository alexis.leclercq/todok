import React, { Component } from 'react';
import { ImageBackground, TouchableOpacity, View, Text, TextInput, Keyboard, Alert, Image } from 'react-native';
import { height, width } from '../tools/Layout';
import firebase from '../tools/firebase';

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export default class LoginScreen extends Component {

    state = {
        signup: true,
        email: null,
        password: null,
    };

    async signInWithLoginEmail() {
        if (!validateEmail(this.state.email)) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        if (!this.state.password) {
            Alert.alert('Merci d\'entrer un mot de passe valide');
            return;
        }
        await firebase
            .auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .then((e) => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email, this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        const user = firebase.auth().currentUser;

                        if (!user.uid) {
                            Alert.alert('Merci d\'essayer de vous reconnecter');
                            return;
                        }
                        const id1 = Math.random().toString(36).substring(3);
                        const id2 = Math.random().toString(36).substring(3);
                        const ref = 'local' + user.uid + 'Todok' + id1 + id2;
                        await firebase.firestore().collection('users')
                            .doc(user.uid).get().then(async (docRef) => {
                                if (docRef.exists)
                                    await firebase.firestore().collection('users')
                                        .doc(user.uid).update({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                        .then(() => {
                                            this.props.navigation.navigate('Main', { session: ref })
                                        });
                            })
                    })
                    .catch((error) => {
                        Alert.alert('Une erreur est survenue')
                    });
            }).catch(error => {
                let errorCode = error.code;
                let errorMessage = error.message;
                if (errorCode === 'auth/weak-password') {
                    Alert.alert('Le mot de passe est incorrect')
                } else {
                    Alert.alert(errorMessage)
                }
            });
    }

    async signInWithEmail() {
        console.log("MAIL")
        if (!validateEmail(this.state.email)) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((e) => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email, this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        const user = firebase.auth().currentUser;

                        if (!user.uid) {
                            Alert.alert('Merci d\'essayer de vous reconnecter');
                            return;
                        }
                        const id1 = Math.random().toString(36).substring(3);
                        const id2 = Math.random().toString(36).substring(3);
                        const ref = 'local' + user.uid + 'Todok' + id1 + id2;
                        await firebase.firestore().collection('users')
                            .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                            .then(() => {
                                this.props.navigation.state.params.analytics.track("Signed up", { "None": "None" });
                                this.props.navigation.navigate('Main', { session: ref })
                            }).catch(err => console.log(err));
                    })
                    .catch((error) => {
                        console.log(error)
                        Alert.alert('Une erreur est survenue')
                    });
            }).catch((error) => {
                if (String(error).includes('Password')) {
                    Alert.alert('Votre mot de passe doit contenir au moins 6 caractères.');
                    return;
                }
                if (String(error).includes('email')) {
                    Alert.alert('Votre email a déjà été utilisée');
                    return;
                }
                Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊');
            });
    }

    render() {
        return (
            <ImageBackground source={require('../assets/defaultback.png')} style={{ height: '100%', width: '100%', alignItems: 'center' }}>
                <TouchableOpacity activeOpacity={100} activeOpacity={100} onPress={Keyboard.dismiss} style={{
                    position: 'absolute',
                    height: height * 1,
                    width: width,
                    backgroundColor: 'rgba(0, 0, 0, 0.05)',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <View style={{
                        width: width * 0.9,
                        backgroundColor: '#FFF',
                        bottom: '5%',
                        justifyContent: 'center',
                        borderRadius: 15,
                        opacity: 1,
                        shadowColor: "#000", shadowOffset: { width: 0, height: 0, }, shadowOpacity: 0.10, shadowRadius: 7, elevation: 7
                    }}>
                        {this.state.signup ? (
                            <>
                                <View style={{ padding: 30 }}>
                                    <Text style={{ textAlign: 'left', paddingTop: 10, fontSize: 18, fontFamily: 'avenirblack', color: '#0094FF' }}>E-mail</Text>
                                    <TouchableOpacity onPress={() => this.emailInput.focus()} activeOpacity={1} style={{ marginTop: 10, width: '100%', padding: 20, borderRadius: 10, shadowColor: "#000", backgroundColor: '#fff', shadowOffset: { width: 0, height: 0, }, shadowOpacity: 0.10, shadowRadius: 7, elevation: 7 }}>
                                        <TextInput onSubmitEditing={() => this.passwordInput.focus()} ref={(input) => this.emailInput = input} style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4' }} onChangeText={(e) => this.setState({ email: e })} placeholder={"E-mail"} value={this.state.email} autoCapitalize={"none"} />
                                    </TouchableOpacity>
                                    <Text style={{ textAlign: 'left', paddingTop: 20, fontSize: 18, fontFamily: 'avenirblack', color: '#0094FF' }}>Mot de passe</Text>
                                    <TouchableOpacity onPress={() => this.passwordInput.focus()} activeOpacity={1} style={{ marginTop: 10, width: '100%', padding: 20, borderRadius: 10, shadowColor: "#000", backgroundColor: '#fff', shadowOffset: { width: 0, height: 0, }, shadowOpacity: 0.10, shadowRadius: 7, elevation: 7 }}>
                                        <TextInput onSubmitEditing={() => this.signInWithEmail()} secureTextEntry ref={(input) => this.passwordInput = input} style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4' }} onChangeText={(e) => this.setState({ password: e })} placeholder={"Mot de passe"} value={this.state.password} autoCapitalize={"none"} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState({ signup: false })}>
                                        <Text style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4', marginTop: 30 }}>J'ai déjà un compte</Text>
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity onPress={() => this.signInWithEmail()} style={{ backgroundColor: '#0094FF', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, padding: 20 }}>
                                    <Text style={{ textAlign: 'center', fontSize: 18, fontFamily: 'avenirbook', color: '#fff' }}>Créer ma première liste 👉</Text>
                                </TouchableOpacity>
                            </>
                        ) : (
                                <>
                                    <View style={{ padding: 30 }}>
                                        <Text style={{ textAlign: 'left', paddingTop: 10, fontSize: 18, fontFamily: 'avenirblack', color: '#0094FF' }}>E-mail</Text>
                                        <TouchableOpacity onPress={() => this.emailInput.focus()} activeOpacity={1} style={{ marginTop: 10, width: '100%', padding: 20, borderRadius: 10, shadowColor: "#000", backgroundColor: '#fff', shadowOffset: { width: 0, height: 0, }, shadowOpacity: 0.10, shadowRadius: 7, elevation: 7 }}>
                                            <TextInput onSubmitEditing={() => this.passwordInput.focus()} ref={(input) => this.emailInput = input} style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4' }} onChangeText={(e) => this.setState({ email: e })} placeholder={"E-mail"} value={this.state.email} autoCapitalize={"none"} />
                                        </TouchableOpacity>
                                        <Text style={{ textAlign: 'left', paddingTop: 20, fontSize: 18, fontFamily: 'avenirblack', color: '#0094FF' }}>Mot de passe</Text>
                                        <TouchableOpacity onPress={() => this.passwordInput.focus()} activeOpacity={1} style={{ marginTop: 10, width: '100%', padding: 20, borderRadius: 10, shadowColor: "#000", backgroundColor: '#fff', shadowOffset: { width: 0, height: 0, }, shadowOpacity: 0.10, shadowRadius: 7, elevation: 7 }}>
                                            <TextInput onSubmitEditing={() => this.signInWithLoginEmail()} secureTextEntry ref={(input) => this.passwordInput = input} style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4' }} onChangeText={(e) => this.setState({ password: e })} placeholder={"Mot de passe"} value={this.state.password} autoCapitalize={"none"} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({ signup: true })}>
                                            <Text style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4', marginTop: 30 }}>Je n'ai pas encore de compte</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity onPress={() => this.signInWithLoginEmail()} style={{ backgroundColor: '#0094FF', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, padding: 20 }}>
                                        <Text style={{ textAlign: 'center', fontSize: 18, fontFamily: 'avenirbook', color: '#fff' }}>Se connecter 👉</Text>
                                    </TouchableOpacity>
                                </>
                            )}
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ marginLeft: 40, marginTop: 70, width, height: 30 }}>
                    <Image source={require('../assets/goback.png')} resizeMode="contain" style={{ height: 30 }} />
                </TouchableOpacity>
            </ImageBackground>
        );
    }
}

LoginScreen.navigationOptions = {
    header: null,
    gesturesEnabled: false,
};