import React, { Component } from 'react';
import { Image } from 'react-native';
import { height, width } from '../tools/Layout';
import firebase from '../tools/firebase';

export default class LoadingScreen extends Component {
    async componentDidMount() {
        firebase.auth().onAuthStateChanged(async (user) => {
            if (user != null) {
                const id1 = Math.random().toString(36).substring(3);
                const id2 = Math.random().toString(36).substring(3);
                const ref = 'local' + user.uid + 'Todok' + id1 + id2;
                this.props.navigation.navigate('Main', { session: ref });
            } else {
                this.props.navigation.navigate('First');
            }
        });
    }

    signOut = async () => {
        firebase.auth().signOut().then(() => {
            this.setState({ showMore: false, swipeablePanelActive: false });
            this.props.navigation.navigate('First')
        }, function (error) {
            // An error happened.
        });
    };

    render() {
        return (
            <Image style={{ height, width }} source={require('../assets/splash.png')} />
        );
    }
}

LoadingScreen.navigationOptions = {
    header: null
};