import React, { Component } from 'react';
import { Image, SafeAreaView, StyleSheet, View, Text, TouchableOpacity, TextInput, Platform, Alert, ImageBackground, FlatList, Linking, AsyncStorage } from 'react-native';
import CheckBox from 'react-native-check-box';
import { height, width } from '../tools/Layout';
import firebase from '../tools/firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SwipeablePanel from 'rn-swipeable-panel';
import CustomCarousel from '../components/CustomCarousel';
import ExpoMixpanelAnalytics from '@benawad/expo-mixpanel-analytics';
import * as SQLite from 'expo-sqlite';
import moment from 'moment';
import { getUserLocalData, getUserOnlineData } from '../api/Users';
import { addCard, editCard, removeCard } from '../api/Cards';
import * as StoreReview from 'expo-store-review';
import { BlurView } from 'expo-blur';
import Confetti from 'react-native-confetti';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { AnimatedSVGPath } from "react-native-svg-animations";
import ImageSVG from 'react-native-remote-svg';

const analytics = new ExpoMixpanelAnalytics('b1baf3361ff286c515c936ea19262112')

analytics.track("Open app", { "Referred By": "Friend" });

const db = SQLite.openDatabase('todok.db');

const animations = [
    require('../assets/animation/todokAnim_spin_reverse_spinning_2.svg'),
    require('../assets/animation/todokAnim_smile.svg'),
    require('../assets/animation/todokAnim_jump.svg'),
    require('../assets/animation/todokAnim_cleinDoeil.svg'),
    require('../assets/animation/todokAnim_spin_reverse_spinning_3.svg'),
    require('../assets/animation/todokAnim_spin_reverse_spinning_1.svg'),
    require('../assets/animation/todokAnim_spin_diagonal.svg'),
    require('../assets/animation/todokAnim_spin_reverse.svg')
];

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}  

export default class MainScreen extends Component {

    state = {
        user: undefined,
        lists: false,
        addCard: false,
        cardType: 'calendar',
        cardName: undefined,
        swipeablePanelActive: false,
        showMore: false,
        showMoreTodok: false,
        selected: 0,
        editing: false,
        archive: true,
        localData: null,
        hasInternet: false,
        img: null,
        backActiveFirst: true,
        carouselItems: [
            {
                title: "Item 1",
                text: "Text 1",
            },
            {
                title: "Item 2",
                text: "Text 2",
            },
            {
                title: "Item 3",
                text: "Text 3",
            },
            {
                title: "Item 4",
                text: "Text 4",
            },
            {
                title: "Item 5",
                text: "Text 5",
            },
        ],
        newSession: false,
        time: 'day',
    };

    async componentDidMount() {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        db.transaction(tx => {
            tx.executeSql(
                "create table if not exists users (archive BOOLEAN DEFAULT NULL, last_updated text, lists text, background text);"
            );
        });
        db.transaction(tx => {
            tx.executeSql("select * from users", [], async (_, { rows }) => {
                if (rows.length >= 1 && rows.item(0).background)
                    this.setState({ img: rows.item(0).background});
                else {
                    await firebase.storage().ref().child('backgrounds/' + user.uid).getDownloadURL().then((val) => {
                        if (val)
                            this.setState({ img: val });
                    }).catch((err) => null);
                }
                await firebase.firestore().collection('users')
                    .doc(user.uid)
                    .get()
                    .then(async (docRef) => {
                        this.setState({ hasInternet: true });
                        if (rows.length > 0 && rows.item(0) && rows.item(0).last_updated != undefined && rows.item(0).lists != undefined) {
                            if (docRef.data() && docRef.data().last_updated && moment(docRef.data().last_updated).isBefore(moment(rows.item(0).last_updated))) {
                                console.log("LOCAL DATA NO UPDATE")
                                this.setState({ localData: true, newSession: false });
                                let res = await getUserLocalData(db, true, user);
                                this.setState({ archive: res.archive, lists: res.lists });
                            } else {
                                console.log("LOCAL DATA NEED UPDATE")
                                this.setState({ localData: false, newSession: false });
                                let res = await getUserOnlineData(db, user, docRef);
                                this.setState({ archive: res.archive, lists: res.lists });
                            }
                        } else {
                            if (docRef.exists) {
                                console.log("ONLINE DATA")
                                this.setState({ localData: false, newSession: false });
                                let res = await getUserOnlineData(db, user, docRef);
                                this.setState({ archive: res.archive, lists: res.lists });
                            } else {
                                console.log("ONLINE DATA CREATING ACCOUNT")
                                await firebase.firestore().collection('users')
                                    .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                    .then(async () => {
                                        this.setState({ localData: false, newSession: true });
                                        let res = await getUserOnlineData(db, user, docRef);
                                        this.setState({ archive: res.archive, lists: res.lists });
                                    });
                            }
                        }
                    })
                    .catch(async (err) => {
                        console.log(err)
                        this.setState({ hasInternet: false, newSession: false });
                        AsyncStorage.setItem("todokDBNeedUpdate", "true");
                        if (rows.length > 0 && rows.item(0) && rows.item(0).last_updated != undefined && rows.item(0).lists != undefined) {
                            console.log("LOCAL DATA")
                            this.setState({ localData: true });
                            let res = await getUserLocalData(db, false, user);
                            this.setState({ archive: res.archive, lists: res.lists });
                        } else
                            Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                    });
            }
            );
        });
    }

    async componentDidUpdate(prevProps) {
        if (this.props === prevProps)
            return;
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        db.transaction(tx => {
            tx.executeSql(
                "create table if not exists users (archive BOOLEAN DEFAULT NULL, last_updated text, lists text, background text);"
            );
        });
        db.transaction(tx => {
            tx.executeSql("select * from users", [], async (_, { rows }) => {
                if (rows.length >= 1 && rows.item(0).background)
                    this.setState({ img: rows.item(0).background});
                else {
                    await firebase.storage().ref().child('backgrounds/' + user.uid).getDownloadURL().then((val) => {
                        if (val)
                            this.setState({ img: val });
                    }).catch((err) => null);
                }
                await firebase.firestore().collection('users')
                    .doc(user.uid)
                    .get()
                    .then(async (docRef) => {
                        this.setState({ hasInternet: true });
                        if (rows.length > 0 && rows.item(0) && rows.item(0).last_updated != undefined && rows.item(0).lists != undefined) {
                            if (docRef.data() && docRef.data().last_updated && moment(docRef.data().last_updated).isBefore(moment(rows.item(0).last_updated))) {
                                console.log("LOCAL DATA NO UPDATE")
                                this.setState({ localData: true, newSession: false });
                                let res = await getUserLocalData(db, true, user);
                                this.setState({ archive: res.archive, lists: res.lists });
                            } else {
                                console.log("LOCAL DATA NEED UPDATE")
                                this.setState({ localData: false, newSession: false });
                                let res = await getUserOnlineData(db, user, docRef);
                                this.setState({ archive: res.archive, lists: res.lists });
                            }
                        } else {
                            if (docRef.exists) {
                                console.log("ONLINE DATA")
                                this.setState({ localData: false, newSession: false });
                                let res = await getUserOnlineData(db, user, docRef);
                                this.setState({ archive: res.archive, lists: res.lists });
                            } else {
                                console.log("ONLINE DATA CREATING ACCOUNT")
                                await firebase.firestore().collection('users')
                                    .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                    .then(async () => {
                                        this.setState({ localData: false, newSession: true });
                                        let res = await getUserOnlineData(db, user, docRef);
                                        this.setState({ archive: res.archive, lists: res.lists });
                                    });
                            }
                        }
                    })
                    .catch(async (err) => {
                        console.log(err)
                        this.setState({ hasInternet: false, newSession: false });
                        AsyncStorage.setItem("todokDBNeedUpdate", "true");
                        if (rows.length > 0 && rows.item(0) && rows.item(0).last_updated != undefined && rows.item(0).lists != undefined) {
                            console.log("LOCAL DATA")
                            this.setState({ localData: true });
                            let res = await getUserLocalData(db, false, user);
                            this.setState({ archive: res.archive, lists: res.lists });
                        } else
                            Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                    });
            }
            );
        });
    }

    updateSession() {
        this.setState({ newSession: false });
    }

    uriToBlob = (uri) => {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onload = function () {
                // return the blob
                resolve(xhr.response);
            };

            xhr.onerror = function () {
                // something went wrong
                reject(new Error('uriToBlob failed'));
            };
            // this helps us get a blob
            xhr.responseType = 'blob';
            xhr.open('GET', uri, true);

            xhr.send(null);
        });
    }

    uploadToFirebase = (blob) => {
        const user = firebase.auth().currentUser;
        return new Promise((resolve, reject) => {
            var storageRef = firebase.storage().ref();
            storageRef.child('backgrounds/'+ user.uid).put(blob, {
                contentType: 'image/jpeg'
            }).then((snapshot) => {
                blob.close();
                resolve(snapshot);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    handleChoosePhoto = async () => {
        const gallery = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const hasGalleryPermission = (gallery.status === 'granted');
        if (hasGalleryPermission === null || hasGalleryPermission === false) {
            Alert.alert("Merci d'autoriser l'accès à vos photos pour changer le fond d'écran");
            return;
        }
        ImagePicker.launchImageLibraryAsync({
            mediaTypes: "Images"
        }).then((result) => {
            if (!result.cancelled) {
                const { height, width, type, uri } = result;
                db.transaction(
                    tx => {
                        tx.executeSql("update users set background = (?)", [uri]);
                    },
                    null,
                );
                this.setState({ img: uri });
                return this.uriToBlob(uri);
            }
        }).then((blob) => {
            return this.uploadToFirebase(blob);
        }).then((snapshot) => {
            console.log("File uploaded");
            analytics.track("Edit-Back", { "None": "None" });
        }).catch((error) => {
            throw error;
        });
    }

    renderFooter = () => {
        return <TouchableOpacity activeOpacity={100} onPress={() => this.setState({ addCard: true })}><Image
            source={require('../assets/addcard.png')} resizeMode={"contain"}
            style={{ height: 20, width: 20, marginLeft: 20, marginRight: 20 }} /></TouchableOpacity>;
    };

    openPanel = () => {
        analytics.track("Open me", { "None": "None" });
        this.setState({ swipeablePanelActive: true });
    };

    closePanel = () => {
        this.setState({ swipeablePanelActive: false });
    };

    signOut = async () => {
        db.transaction(tx => {
            tx.executeSql("drop table if exists users");
            tx.executeSql("drop table if exists lists");
            tx.executeSql("drop table if exists pages");
            tx.executeSql("drop table if exists bubbles");
        });
        this.setState({ showMore: false, swipeablePanelActive: false, user: undefined, lists: false, addCard: false, cardType: 'calendar', cardName: undefined, selected: 0, editing: false, archive: true, localData: false, hasInternet: false, img: null, carouselItems: [] });
        this.props.navigation.navigate('First');
        await firebase.auth().signOut();
    };

    async addCard() {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        if (this.state.cardName === undefined) {
            Alert.alert('Merci de donner un nom');
            return;
        }
        let res = await addCard(this.state, user, db);
        this.setState({ lists: res.lists, selected: res.selected, cardName: res.cardName, cardType: res.cardType, addCard: res.addCard });
        analytics.track("Add Card", { "None": "None" });
    }

    async removeCard() {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        let res = removeCard(this.state, user, db);
        this.setState({ lists: res.lists, selected: res.selected, editing: res.editing });
        analytics.track("Del Card", { "None": "None" });
    }

    editList(e, id) {
        let { lists } = this.state;

        lists[id].name = e;
        this.setState({ lists });
    }

    async endEditingList(id) {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        let res = editCard(this.state, id, db);
        this.setState({ selected: res.selected, editing: res.editing });
        analytics.track("Edit Card", { "None": "None" });
    }

    async archive(e) {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        db.transaction(
            tx => {
                tx.executeSql("update users set archive = (?)", [e]);
            },
            null,
        );
        await firebase.firestore().collection('users').doc(user.uid).update({ archive: e }).catch(err => console.log(err));
        this.setState({ archive: e });
    }

    render() {
        return (
            <ImageBackground source={this.state.img ? { uri: this.state.img } : require('../assets/empty_back.png')} style={{ height: height, width }}
                resizeMode={'cover'}>
                <Confetti ref={(node) => this._confettiView = node} duration={2000} timeout={1} confettiCount={50} colors={["rgb(255, 112, 112)", "rgb(255, 190, 38)", "rgb(22, 180, 118)", "rgb(0, 148, 255)", "rgb(130, 53, 255)"]} />
                <SafeAreaView style={styles.container}>
                    <View style={{ flex: 4, width: width }}>
                        <View style={{ flexDirection: 'row', flex: 1 }}>
                        <TouchableOpacity activeOpacity={100} onPress={() => this.openPanel()}
                            style={{ justifyContent: 'center', marginRight: 10 }}>
                            <Image source={require('../assets/menu.png')} resizeMode={"contain"}
                                style={{ height: 20, width: 20, marginRight: 10, marginLeft: 30 }} />
                        </TouchableOpacity>
                        {!this.state.lists || this.state.lists === false || this.state.lists.length < 0 ? <View /> : (
                            <FlatList style={{ flex: 1, width }} ListFooterComponent={this.renderFooter} horizontal
                                showsHorizontalScrollIndicator={false} data={this.state.lists}
                                keyExtractor={(item) => item.id.toString()}
                                contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}
                                renderItem={({ item }) => (
                                    <TouchableOpacity activeOpacity={100} key={item.id} onLongPress={() => {
                                        this.setState({
                                            editing: true,
                                            selected: item.id
                                        })
                                    }} onPress={() => {
                                        this.setState({ selected: item.id });
                                        analytics.track("Change-card", { "None": "None" });
                                    }} style={{
                                        marginHorizontal: 10,
                                        justifyContent: 'center',
                                        height: 50,
                                        alignItems: 'center',
                                        backgroundColor: 'rgba(255, 255, 255, 0.95)',
                                        paddingHorizontal: 30,
                                        borderRadius: 50,
                                        borderColor: '#dcdcdc'
                                    }}>
                                        {this.state.editing && this.state.selected === item.id ? (
                                            <View style={{
                                                justifyContent: 'center',
                                                height: 50,
                                                alignItems: 'center',
                                                flexDirection: 'row'
                                            }}>
                                                <TextInput onEndEditing={() => this.endEditingList(item.id)}
                                                    ref={"list" + item.id.toString()} value={item.name}
                                                    onChangeText={e => this.editList(e, item.id)} style={{
                                                        fontSize: 20,
                                                        textAlign: 'center',
                                                        marginTop: 3,
                                                        fontFamily: 'avenirblack',
                                                        color: 'rgba(0, 0, 0, 0.4)',
                                                        marginRight: 20
                                                    }} placeholder={'Donner mon un nom'} />
                                                <TouchableOpacity activeOpacity={100} onPress={() => this.removeCard()} style={{
                                                    height: 50,
                                                    paddingLeft: 20,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    borderLeftWidth: 1,
                                                    borderLeftColor: '#C4C4C4'
                                                }}>
                                                    <Image source={require('../assets/bin.png')}
                                                        resizeMode={"contain"}
                                                        style={{ height: 20, width: 20 }} />
                                                </TouchableOpacity>
                                            </View>
                                        )
                                            : <Text style={{
                                                textAlign: 'center',
                                                fontSize: 20,
                                                marginTop: 3,
                                                color: this.state.selected.toString() === item.id.toString() ? 'rgba(0, 0, 0, 0.4)' : 'rgba(0, 0, 0, 0.2)',
                                                fontFamily: this.state.selected.toString() === item.id.toString() ? 'avenirblack' : 'avenirbook'
                                            }} adjustsFontSizeToFit>{item.name}</Text>}
                                    </TouchableOpacity>
                                )} />
                        )}
                        </View>
                        <View style={{ flexDirection: 'row', flex: 1, marginLeft: 30, marginTop: 10, marginBottom: -20 }}>
                            <TouchableOpacity onPress={() => this.setState({ time: 'day' })} style={{ alignItems: 'center' }}>
                            <Text style={{
                                textAlign: 'center',
                                fontSize: 20,
                                color: 'rgba(255, 255, 255, 0.95)',
                                fontFamily: 'avenirbook',
                            }} adjustsFontSizeToFit>Jour</Text>
                            {this.state.time === 'day' && <Text style={{ textAlign: 'center', fontSize: 17, color: 'rgba(255, 255, 255, 0.95)', fontFamily: 'avenirbook', marginTop: -3 }} adjustsFontSizeToFit>•</Text>}
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ time: 'week' })} style={{ alignItems: 'center' }}>
                            <Text style={{
                                textAlign: 'center',
                                fontSize: 20,
                                color: 'rgba(255, 255, 255, 0.95)',
                                fontFamily: 'avenirbook',
                                marginLeft: 20,
                            }} adjustsFontSizeToFit>Semaine</Text>
                            {this.state.time === 'week' && <Text style={{ textAlign: 'center', fontSize: 17, color: 'rgba(255, 255, 255, 0.95)', fontFamily: 'avenirbook', marginTop: -3, marginLeft: 7 }} adjustsFontSizeToFit>•</Text>}
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ time: 'month' })} style={{ alignItems: 'center' }}>
                            <Text style={{
                                textAlign: 'center',
                                fontSize: 20,
                                color: 'rgba(255, 255, 255, 0.95)',
                                fontFamily: 'avenirbook',
                                marginLeft: 20,
                            }} adjustsFontSizeToFit>Mois</Text>
                            {this.state.time === 'month' && <Text style={{ textAlign: 'center', fontSize: 17, color: 'rgba(255, 255, 255, 0.95)', fontFamily: 'avenirbook', marginTop: -3, marginLeft: 14 }} adjustsFontSizeToFit>•</Text>}
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 21 }}>
                        <CustomCarousel time={this.state.time} updateSession={() => this.updateSession()} newSession={this.state.newSession} confettiRef={this._confettiView} selected={this.state.selected} lists={this.state.lists} analytics={analytics} localData={this.state.localData} archive={this.state.archive} />
                    </View>
                    {this.state.addCard &&
                        <TouchableOpacity activeOpacity={100} activeOpacity={100} onPress={() => this.setState({ addCard: false })} style={{
                            position: 'absolute',
                            height: height * 2,
                            width: width,
                            backgroundColor: 'rgba(0, 0, 0, 0.05)',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <View style={{
                                width: width * 0.8,
                                backgroundColor: '#FFF',
                                bottom: '5%',
                                justifyContent: 'center',
                                borderRadius: 15,
                                opacity: 1,
                            }}>
                                <KeyboardAwareScrollView scrollEnabled={false}>
                                    <View style={{
                                        paddingVertical: 20,
                                        paddingHorizontal: 20,
                                        justifyContent: 'center',
                                        flex: 4
                                    }}>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            borderBottomWidth: 0.5,
                                            borderBottomColor: '#C4C4C4'
                                        }}>
                                            <TouchableOpacity activeOpacity={100} onPress={() => this.refs.cardField.focus()}
                                                activeOpacity={100}
                                                style={{ flex: 2, backgroundColor: '#fff' }}>
                                                <TextInput onSubmitEditing={() => this.addCard()}
                                                    value={this.state.cardName}
                                                    onChangeText={e => this.setState({ cardName: e })} ref='cardField'
                                                    style={{ width: '80%', fontSize: 20, fontFamily: 'avenirbook' }}
                                                    placeholder={'Donne moi un nom'} />
                                            </TouchableOpacity>
                                            <View style={{ justifyContent: 'flex-end', marginTop: -15 }}>
                                                <TouchableOpacity activeOpacity={100} onPress={() => this.addCard()} style={{
                                                    width: '100%',
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }}>
                                                    <Image style={{ height: 40, width: 40, marginTop: 4 }}
                                                        resizeMode={"contain"} source={require('../assets/sendbig.png')} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <CheckBox
                                                checkBoxColor='#C4C4C4'
                                                checkedCheckBoxColor='#0094FF'
                                                checkedImage={null}
                                                style={{ flex: 1, padding: 5, paddingTop: 20 }}
                                                onClick={() => {
                                                    this.setState({
                                                        cardType: 'calendar'
                                                    })
                                                }}
                                                isChecked={this.state.cardType == 'calendar'}
                                                rightText={"Calendrier"}
                                                rightTextStyle={{ color: '#C4C4C4', fontFamily: 'avenirbook', fontSize: 15 }}
                                            />
                                            <CheckBox
                                                checkBoxColor='#C4C4C4'
                                                checkedCheckBoxColor='#0094FF'
                                                style={{ flex: 1, padding: 5 }}
                                                onClick={() => {
                                                    this.setState({
                                                        cardType: 'perso'
                                                    })
                                                }}
                                                isChecked={this.state.cardType == 'perso'}
                                                rightText={"Personnalisé"}
                                                rightTextStyle={{ color: '#C4C4C4', fontFamily: 'avenirbook', fontSize: 15 }}
                                            />
                                        </View>
                                    </View>
                                </KeyboardAwareScrollView>
                            </View>
                        </TouchableOpacity>}
                    <SwipeablePanel style={{ width: width * 0.9, borderRadius: 25, backgroundColor: 'rgba(255, 255, 255, 0.95)' }} isActive={this.state.swipeablePanelActive} closeOnTouchOutside noBackgroundOpacity
                        noBar onClose={() => this.closePanel()} onlyLarge>
                        <BlurView intensity={70} tint={"light"} style={{ padding: 20, borderRadius: 25, height: '200%' }}>
                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => this.setState({ backActiveFirst: true })} style={{ backgroundColor: this.state.backActiveFirst ? '#019AF9' : 'rgba(0, 0, 0, 0.0)', justifyContent: 'center', alignItems: 'center', borderRadius: 100 }}>
                                    <Text style={{
                                        paddingHorizontal: 20,
                                        paddingTop: 10,
                                        paddingBottom: 5,   
                                        textAlign: 'center',
                                        fontSize: 18,
                                        color: this.state.backActiveFirst ? '#fff' : 'rgba(0, 0, 0, 0.4)',
                                        fontFamily: 'avenirblack',
                                    }} adjustsFontSizeToFit>Todok</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ backActiveFirst: false })} style={{ backgroundColor: !this.state.backActiveFirst ? '#019AF9' : 'rgba(0, 0, 0, 0.0)', justifyContent: 'center', alignItems: 'center', borderRadius: 100 }}>
                                    <Text style={{
                                        paddingHorizontal: 20,
                                        paddingTop: 10,
                                        paddingBottom: 5,
                                        textAlign: 'center',
                                        fontSize: 18,
                                        color: !this.state.backActiveFirst ? '#fff' : 'rgba(0, 0, 0, 0.4)',
                                        fontFamily: 'avenirblack',
                                    }} adjustsFontSizeToFit>Autres</Text>
                                </TouchableOpacity>
                            </View>
                            {this.state.backActiveFirst ? 
                            <>
                                <View style={{ width: '100%', alignItems: 'center', marginTop: 20 }}>
                                    <ImageBackground source={require('../assets/poster.png')} resizeMode="contain" style={{ width: '100%', height: height * 0.2, marginRight: '10%', justifyContent: 'center', alignItems: 'center' }} >
                                        <ImageSVG source={animations[getRandomInt(animations.length)]} style={{ width: 200, height: 200 }} />
                                    </ImageBackground>
                                    <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirbook',
                                    marginTop: 40,
                                }} adjustsFontSizeToFit>Todok pourra bientôt manger les confettis de vos pastilles lorsque vous allez les archiver.</Text>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirbook',
                                    marginTop: 20,
                                }} adjustsFontSizeToFit>Attention, il perdra de la vie s'il n'est pas assez nourrit.</Text>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirbook',
                                    marginTop: 20,
                                }} adjustsFontSizeToFit>En attendant, vous pouvez cliquer sur le todok pour jouer avec lui.</Text>
                                <TouchableOpacity activeOpacity={100} onPress={() => Linking.openURL('https://www.todok.co/contact')}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    marginTop: 30,
                                }} adjustsFontSizeToFit>Une question ? Contactez-nous →</Text>
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={100} onPress={() => this.setState({ showMoreTodok: !this.state.showMoreTodok })}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    marginTop: 30,
                                }} adjustsFontSizeToFit>Conseils pratiques <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirbook',
                                }} adjustsFontSizeToFit>sur l'utilisation de l'application</Text> <Image source={this.state.showMoreTodok ? require('../assets/down.png') : require('../assets/right.png')} resizeMode="contain" style={{ height: 15, width: 15, alignSelf: 'center' }} /></Text>
                                </TouchableOpacity>
                                {this.state.showMoreTodok && (
                                    <>
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        fontFamily: 'avenirbook',
                                        marginTop: 10,
                                        width: '90%'
                                    }} adjustsFontSizeToFit>- Maintenir le rond d'une tâche pour supprimer la tâche.</Text>
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        width: '90%',
                                        fontFamily: 'avenirbook',
                                    }} adjustsFontSizeToFit>- Pour ajouter un espace cliquer sur le + en haut à droite.</Text>
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        width: '90%',
                                        fontFamily: 'avenirbook',
                                    }} adjustsFontSizeToFit>- Un espace personnalisé vous permet d'avoir des tâches dont vous aller choisir les catégories.</Text>
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        width: '90%',
                                        fontFamily: 'avenirbook',
                                    }} adjustsFontSizeToFit>- Dans un espace calendrier vous pouvez swipper à gauche de "aujourd'hui" pour accéder à d'autres éléments (A faire, objectifs, etc)</Text>
                                    </>
                                )}
                                </View>
                            </>
                            : 
                            <>
                            <TouchableOpacity activeOpacity={100} onPress={() => this.handleChoosePhoto()}>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    marginTop: 10,
                                    marginBottom: 20
                                }} adjustsFontSizeToFit>Fond d'écran →</Text>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={100} onPress={() => Linking.openURL('https://www.todok.co/contact')}>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    marginTop: 20
                                }} adjustsFontSizeToFit>Des idées d'améliorations ?</Text>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    paddingBottom: 15,
                                }} adjustsFontSizeToFit>→ Envoyer mon idée 🤙</Text>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={100} onPress={() => StoreReview.requestReview()}>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    marginTop: 15
                                }} adjustsFontSizeToFit>Vous ADOREZ todok ?</Text>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    paddingBottom: 15,
                                }} adjustsFontSizeToFit>→ Mettre 5 étoiles 😍</Text>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={100} onPress={() => this.setState({ showMore: !this.state.showMore })}>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirblack',
                                    marginTop: 15,
                                    justifyContent: 'center'
                                }} adjustsFontSizeToFit numberOfLines={2}>Notre histoire en moins d'une minute <Image source={this.state.showMore ? require('../assets/down.png') : require('../assets/right.png')} resizeMode="contain" style={{ height: 15, width: 15, alignSelf: 'center' }} /></Text>
                                {!this.state.showMore && (
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 14,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        fontFamily: 'avenirbook',
                                        paddingBottom: 20,
                                    }} adjustsFontSizeToFit>(Vous resterez sur l'app)</Text>
                                )}
                            </TouchableOpacity>
                            <>
                            {this.state.showMore && (
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 14,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontFamily: 'avenirbook',
                                    paddingBottom: 20,
                                }} adjustsFontSizeToFit>Des todo list il y en a plein !
                                Mais aucune ne vous pousse réellement à ne pas procrastiner. 
                                Voici notre ambition : créer une todo simple et claire qui vous aidera à atteindre vos objectifs. "En voyant ma soeur jouer au tamagotshi, je me suis dit, "c’est exactement ça qu’il nous faut, il faut le mettre dans l’application""Resultat : Todok, c’est un petit personnage sympathique dont il faut prendre soin.
                                À chaque fois que vous archivez une tâche de votre to-do list, des confettis pleuvent et le Todok peut les manger.
                                
                                --
                                
                                Todok est en cours de construction et ne peut pas encore manger les confettis, nous faisons de notre mieux pour apporter cette fonctionnalité le plus rapidement possible. Si le projet vous plait n’hésitez pas à nous encourager ou à nous envoyer vos meilleures idées sur : louisdarques@gmail.com. Fait avec amour par Alexis Leclerq et Louis Darques.</Text>
                            )}
                            </>
                            <View>
                                <TouchableOpacity activeOpacity={100} onPress={() => this.signOut()}>
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        fontFamily: 'avenirblack',
                                        paddingBottom: 20
                                    }} adjustsFontSizeToFit>Déconnexion</Text>
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={100} onPress={() => Linking.openURL('https://www.todok.co/mentions-legales')}>
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: 'rgba(0, 0, 0, 0.7)',
                                        fontFamily: 'avenirblack',
                                        paddingBottom: 20
                                    }} adjustsFontSizeToFit>Légal</Text>
                                </TouchableOpacity>
                            </View>
                            </>
                            }
                        </BlurView>
                    </SwipeablePanel>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

MainScreen.navigationOptions = {
    header: null,
    gesturesEnabled: false,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 7,
    }
});