import * as React from "react"
import Svg, { G, Path, Circle, Defs, ClipPath, Use } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgComponent(props) {
  return (
    <Svg viewBox="0 0 359 359" {...props}>
      <G id="prefix__all_todok">
        <G id="prefix__Corp_Todok">
          <Path
            fill="none"
            stroke="#009aff"
            strokeWidth={63.779}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            d="M232.9 164.1L157 240l-41.1-41.1"
          />
          <Circle cx={115.9} cy={199.1} r={25.6} fill="#008edd" />
        </G>
        <Path
          d="M139.5 235.4h36.7c2.4 0 5 .2 5 5.1 0 12.8-10.4 23.3-23.3 23.3s-23.3-10.4-23.3-23.3c0-4.6 2-5.1 4.9-5.1z"
          opacity={0.8}
          fill="#fff"
          transform="scale(.7 .3)"
        />
        <G id="prefix__Oeuil_gauche_Todok">
          <Defs>
            <Circle id="prefix__SVGID_1_" cx={115.9} cy={199.1} r={25.6} />
          </Defs>
          <ClipPath id="prefix__SVGID_2_">
            <Use xlinkHref="#prefix__SVGID_1_" overflow="visible" />
          </ClipPath>
          <G clipPath="url(#prefix__SVGID_2_)">
            <Circle className="prefix__st4" cx={115.9} cy={199.1} r={25.6} />
            <Circle className="prefix__st5" cx={115.9} cy={199.1} r={19.7} />
            <G>
              <Circle className="prefix__st6" cx={115.9} cy={199.1} r={13.1} />
              <Circle className="prefix__st7" cx={115.9} cy={199.1} r={7.7} />
              <Circle className="prefix__st8" cx={120.5} cy={198.2} r={2.6} />
            </G>
          </G>
        </G>
        <G id="prefix__Oeuil_droit_Todok">
          <Circle className="prefix__st4" cx={232.9} cy={164.1} r={32.1} />
          <Circle className="prefix__st5" cx={232.9} cy={164.1} r={19.6} />
          <G>
            <Circle className="prefix__st6" cx={232.9} cy={164.1} r={13} />
            <Circle className="prefix__st7" cx={232.9} cy={164.1} r={7.7} />
            <Circle className="prefix__st8" cx={237.5} cy={163.2} r={2.6} />
          </G>
        </G>
      </G>
      <Path
        fill="none"
        stroke="#008edd"
        strokeMiterlimit={10}
        d="M0 271.3h359"
      />
    </Svg>
  )
}

export default SvgComponent;