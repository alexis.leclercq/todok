import React, { Component } from 'react';
import { height, width } from "../tools/Layout";
import { View, Image, Text, TouchableOpacity, TextInput, StyleSheet, Platform, FlatList, AsyncStorage, LayoutAnimation, YellowBox, Button } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Overlay } from "react-native-elements";
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
import CheckBox from "react-native-check-box";
import SwipeablePanel from "rn-swipeable-panel";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as ExpoHaptics from 'expo-haptics';
import { BlurView } from 'expo-blur';

YellowBox.ignoreWarnings([
    'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

async function getiOSNotificationPermission() {
    const { status } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    if (status !== 'granted') {
        await Permissions.askAsync(Permissions.NOTIFICATIONS);
    }
}

export default class Bubble extends Component {
    state = {
        item: null,
        editSelected: 'none',
        date: new Date(),
        error: false,
        textEditing: false,
        swipeablePanelActive: false,
        startingIndex: 0,
        showMore: false,
        showMoreAdd: false,
        showMoreDrop: false,
        dateActivate: false,
    };

    async componentDidMount() {
        if (this.props.item) {
            this.getDays(this.props.item);
            if (this.props.item.open && this.props.item.open !== "false") {
                if (this.props.item.note || (this.props.item.tag && this.props.item.tag.length > 0) || (this.props.item.underTask && this.props.item.underTask.length > 0))
                    this.setState({ showMore: true });
                else
                    this.setState({ showMoreAdd: true });
            }
        }
    }

    listenForNotifications = () => {
        Notifications.addListener(notification => {
            if (notification.origin === 'received' && Platform.OS === 'ios') {
                //Alert.alert(notification.origin);
            }
        });
    };

    async sendNotif() {
        const localnotification = {
            title: 'Todok',
            body: this.state.item.name,
            android: {
                sound: true,
            },
            ios: {
                sound: true,
            },
        };
        let date = new Date(this.state.date);

        await getiOSNotificationPermission();
        this.listenForNotifications();
        const schedulingOptions = { time: date };
        let id = Notifications.scheduleLocalNotificationAsync(
            localnotification,
            schedulingOptions
        );
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.item !== this.props.item) {
            this.getDays(this.props.item);
            if (this.props.item.open && this.props.item.open !== "false") {
                if (this.props.item.note || (this.props.item.tag && this.props.item.tag.length > 0) || (this.props.item.underTask && this.props.item.underTask.length > 0))
                    this.setState({ showMore: true });
                else
                    this.setState({ showMoreAdd: true });
            }
        }
    }

    editText(e) {
        let { item } = this.state;
        item.name = e;
        this.setState({ item });
    }

    getArchivedIndex(item) {
        for (let i in item.archived) {
            if (item.archived[i] === this.props.date)
                return i;
        }
        return -1;
    }

    archive() {
        let { item } = this.state;
        if (this.state.textEditing)
            return;
        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
        if (this.hasTrue()) {
            if (item.archived.includes(this.props.date)) {
                let index = this.getArchivedIndex(item);
                if (this.props.date !== moment(new Date()).format('L')) {
                    this.setState({ error: true });
                    return;
                }
                if (!item.archived || !item.archived.length)
                    item.archived = [];
                if (index !== -1) {
                    item.archived.splice(index, 1);
                    if (item.days - 1 >= 0)
                        item.days--;
                }
            } else {
                if (this.props.confettiRef)
                    this.props.confettiRef.startConfetti();
                this.props.analytics.track("Archiv Hab", { "None": "None" });
                let index = this.getArchivedIndex(item);

                if (this.props.date !== moment(new Date()).format('L')) {
                    this.setState({ error: true });
                    return;
                }
                if (!item.archived || !item.archived.length)
                    item.archived = [];
                if (index === -1) {
                    item.archived.push(this.props.date);
                    item.days++;
                }
            }
        } else {
            if (item.archived) {
                item.archived = false;
                this.props.analytics.track("Back-Semi-A", { "None": "None" });
            } else {
                if (this.props.confettiRef)
                    this.props.confettiRef.startConfetti();
                this.props.analytics.track("Semi Archiv", { "None": "None" });
                item.archived = true;
            }
        }
        this.setState({ item });
        this.endEditing().then(r => null);
    }

    delete() {
        if (!this.hasTrue()) {
            if (this.props.confettiRef)
                this.props.confettiRef.startConfetti();
            this.props.deleteBubble(this.state.item.ref);
        }
    }

    editColor(e) {
        let { item } = this.state;
        item.color = e;
        this.props.analytics.track("Edit Color", { "None": "None" });
        this.setState({ item });
        this.endEditing().then(r => null);
    }

    async endEditing() {
        let db = this.props.dataBase;
        if (!this.state.item.page) {
            db.transaction(
                tx => {
                    tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                    tx.executeSql("select * from bubbles where id = (?)", [this.state.item.ref], async (_, { rows }) => {
                        if (rows.length != 0) {
                            tx.executeSql("update bubbles set name = (?), date = (?), archived = (?), days = (?), reminders = (?), repeat = (?), note = (?), tag = (?), underTask = (?), open = (?) where id = (?)", [
                                this.state.item.name,
                                this.state.item.date ? this.state.item.date : moment(new Date()).format('L'),
                                this.state.item.archived ? JSON.stringify(this.state.item.archived) : false,
                                this.state.item.days ? this.state.item.days : 0,
                                this.state.item.reminders ? JSON.stringify(this.state.item.reminders) : null,
                                this.state.item.repeat ? JSON.stringify(this.state.item.repeat) : null,
                                this.state.item.note ? this.state.item.note : null,
                                this.state.item.tag ? JSON.stringify(this.state.item.tag) : null,
                                this.state.item.underTask ? JSON.stringify(this.state.item.underTask) : null,
                                this.state.item.open ? this.state.item.open : "false",
                                this.state.item.ref
                            ]);
                            AsyncStorage.setItem("todokDBNeedUpdate" + this.state.item.ref, "true");
                        }
                    });
                }
            );
        } else {
            db.transaction(
                tx => {
                    tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                    tx.executeSql("select * from bubbles where id = (?)", [this.state.item.ref], async (_, { rows }) => {
                        if (rows.length != 0) {
                            tx.executeSql("update bubbles set name = (?), page = (?), archived = (?), reminders = (?), repeat = (?), note = (?), tag = (?), underTask = (?), open = (?) where id = (?)", [
                                this.state.item.name,
                                this.state.item.page,
                                this.state.item.archived ? JSON.stringify(this.state.item.archived) : false,
                                this.state.item.reminders ? JSON.stringify(this.state.item.reminders) : null,
                                this.state.item.repeat ? JSON.stringify(this.state.item.repeat) : null,
                                this.state.item.note ? this.state.item.note : null,
                                this.state.item.tag ? JSON.stringify(this.state.item.tag) : null,
                                this.state.item.underTask ? JSON.stringify(this.state.item.underTask) : null,
                                this.state.item.open ? this.state.item.open : "false",
                                this.state.item.ref
                            ]);
                            AsyncStorage.setItem("todokDBNeedUpdate" + this.state.item.ref, "true");
                        }
                    });
                }
            );
        }
    }

    addReminder() {
        let { item } = this.state;

        if (!item.reminders)
            item.reminders = [];
        this.props.analytics.track("Ajout Rappel", { "None": "None" });
        item.reminders.push(this.state.date.toUTCString());
        this.setState({ item });
        this.sendNotif();
        this.endEditing().then(r => null);
    }

    removeReminder(key) {
        let { item } = this.state;

        item.reminders.splice(key, 1);
        this.setState({ item });
        this.endEditing().then(r => null);
    }

    getLastDay(item) {
        if (this.hasTrueItem(item)) {
            if (!item.archived || !item.archived.length)
                item.archived = [];
            for (let i = 0; i != 7; i++) {
                item.archived.push(moment(new Date()).subtract(i, 'days').format('L'));
            }
        }
        return item;
    }

    addRepeat(key, state) {
        let { item } = this.state;
        let check = false;

        if (!item.repeat)
            item.repeat = {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false,
                saturday: false,
                sunday: false
            };
        else {
            check = true;
        }
        item.repeat[key] = state;
        if (check === true)
            item = this.getLastDay(item);
        this.props.analytics.track("Ajout Hab", { "None": "None" });
        this.setState({ item });
        this.endEditing().then(r => null);
    }

    hasTrue() {
        for (let i in this.state.item.repeat) {
            if (this.state.item.repeat[i] === true)
                return true;
        }
        return false;
    }

    hasTrueItem(item) {
        for (let i in item.repeat) {
            if (item.repeat[i] === true)
                return true;
        }
        return false;
    }

    getDays(item) {
        if (this.hasTrueItem(item)) {
            if (!item.archived || !item.archived.length)
                item.archived = [];
            let i = 1;
            for (; !this.isRepeated(item, moment(new Date()).locale('en').subtract(i, 'days').format('dddd').toLowerCase()); i++);
            if (!item.archived.includes(moment(new Date()).locale('fr').subtract(i, 'days').format('L'))) {
                item.days = 0;
            }
        }
        this.setState({ item });
    }

    isRepeated(item, date) {
        if (item.repeat[date] === true)
            return true;
        return false;
    }

    openPanel = () => {
        this.setState({ swipeablePanelActive: true });
    };

    closePanel = () => {
        this.setState({ swipeablePanelActive: false });
    };

    addNewUnderTask(e) {
        const { underTask } = this.state.item;

        this.props.analytics.track("add Sous-t", { "None": "None" });
        if (e.nativeEvent.text.length == 0)
            return;
        underTask.push({ id: underTask.length, name: e.nativeEvent.text, archived: false });
        this.setState({ item: { ...this.state.item, underTask } });
    }

    editUnderTask(e, id) {
        const { underTask } = this.state.item;

        if (e.nativeEvent.text.length == 0)
            return;
        underTask[id] = { ...underTask[id], name: e.nativeEvent.text };
        this.setState({ item: { ...this.state.item, underTask } });
    }

    removeUnderTask(id) {
        const { underTask } = this.state.item;

        console.log(underTask)
        underTask.splice(id, 1);
        console.log(underTask)
        this.setState({ item: { ...this.state.item, underTask } });
        this.endEditing();
    }

    addTag(name) {
        const { tag } = this.state.item;

        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
        this.props.analytics.track("add id", { "None": "None" });
        if (tag.includes(name))
            tag.splice(tag.indexOf(name), 1);
        else
            tag.push(name);
        this.setState({ item: { ...this.state.item, tag } });
    }

    changeLayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this.state.item.open && this.state.item.open === "true")
            this.props.analytics.track("Close-p", { "None": "None" });
        else
            this.props.analytics.track("Open-p", { "None": "None" });
        this.setState({ showMoreDrop: false, showMore: !this.state.showMore, item: { ...this.state.item, open: this.state.item.open && this.state.item.open === "true" ? "false" : "true" } });
        this.endEditing();
    }

    changeLayoutAdd = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this.state.item.open && this.state.item.open === "true")
            this.props.analytics.track("Close-p", { "None": "None" });
        else
            this.props.analytics.track("Open-p", { "None": "None" });
        this.setState({ showMoreDrop: false, showMoreAdd: !this.state.showMoreAdd, item: { ...this.state.item, open: this.state.item.open && this.state.item.open === "true" ? "false" : "true" } });
        this.endEditing();
    }

    changeLayoutDrop = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (!this.state.showMoreDrop)
            this.props.analytics.track("Open me-p", { "None": "None" });
        this.setState({ showMoreDrop: !this.state.showMoreDrop, showMore: false, showMoreAdd: false, item: { ...this.state.item } });
        this.endEditing();
    }

    render() {
        if (this.state.item) {
            const { repeat, tag, underTask, name, note } = this.state.item;
            return (
                <View>
                    <View style={{
                        width: width * 0.8,
                        marginLeft: 30,
                        backgroundColor: 'rgba(255, 255, 255, 0.95)',
                        borderRadius: 25,
                        minHeight: 50,
                        borderColor: '#dcdcdc',
                        marginBottom: 13,
                    }}>
                        <BlurView intensity={70} tint={"light"} style={{ borderRadius: 25 }}>
                            <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => null} style={{
                                flexDirection: 'row',
                                paddingLeft: 20,
                            }}>
                                <TouchableOpacity activeOpacity={100} onPress={() => {
                                    if ((this.state.item && this.state.item.note) || (tag && tag.length > 0) || (underTask && underTask.length > 0))
                                        this.changeLayout();
                                    else
                                        this.changeLayoutAdd();
                                }}>
                                    {(this.state.showMoreAdd || this.state.showMoreDrop) ? (
                                        <TextInput onEndEditing={() => this.endEditing()} onChangeText={(e) => this.setState({ item: { ...this.state.item, name: e } })} style={{
                                            alignSelf: 'center',
                                            width: (width * 0.8) - 72,
                                            textAlign: 'left',
                                            textDecorationLine: this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? 'none' : 'line-through' : this.state.item.archived ? 'line-through' : 'none',
                                            fontSize: 17,
                                            color: 'rgba(0, 0, 0, 0.7)',
                                            fontFamily: 'avenirbook',
                                            paddingTop: 3,
                                            paddingRight: 10,
                                            marginVertical: 14
                                        }}>{this.state.item.name}</TextInput>
                                    ) : (
                                            <Text style={{
                                                alignSelf: 'center',
                                                width: (width * 0.8) - 72,
                                                textAlign: 'left',
                                                textDecorationLine: this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? 'none' : 'line-through' : this.state.item.archived ? 'line-through' : 'none',
                                                fontSize: 17,
                                                color: 'rgba(0, 0, 0, 0.7)',
                                                fontFamily: 'avenirbook',
                                                paddingTop: 3,
                                                paddingRight: 10,
                                                marginVertical: 14
                                            }}>{this.state.item.name[this.state.item.name.length - 1] === ' ' ? this.state.item.name : this.state.item.name + " "} {(this.state.item.note || (tag && tag.length > 0) || (underTask && underTask.length > 0)) && <Image source={this.state.showMore ? require('../assets/bubble_up.png') : require('../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} />}</Text>

                                        )}
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={100} onPress={() => this.archive()} onLongPress={() => this.delete()}>
                                    <Image
                                        source={this.state.textEditing ? require('../assets/sendbig.png') : this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? require('../assets/notarchivedbig.png') : require('../assets/archivedbig.png') : this.state.item.archived ? require('../assets/archivedbig.png') : require('../assets/notarchivedbig.png')}
                                        resizeMode={"contain"} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                            </TouchableOpacity>
                            {this.state.showMore && (
                                <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => this.changeLayout()}>
                                    {underTask && underTask.length > 0 &&
                                        <FlatList data={underTask} keyExtractor={item => item.id.toString()} renderItem={({ item, index }) => {
                                            return (
                                                <TouchableOpacity onPress={() => {
                                                    underTask[index].archived = !underTask[index].archived;
                                                    this.setState({ item: { ...this.state.item, underTask } });
                                                    this.endEditing();
                                                }} key={index} style={{ flexDirection: 'row', marginHorizontal: 20, marginTop: 10 }}>
                                                    <Image source={item.archived ? require('../assets/underarchived.png') : require('../assets/not_archived.png')} style={{ height: 17, width: 17 }} resizeMode={"contain"} />
                                                    <Text style={{
                                                        paddingTop: 3,
                                                        textAlign: 'left',
                                                        fontSize: 15,
                                                        color: 'rgba(0, 0, 0, 0.7)',
                                                        fontFamily: 'avenirbook',
                                                        marginLeft: 10
                                                    }}>{item.name}</Text>
                                                </TouchableOpacity>
                                            );
                                        }} />
                                    }
                                    {this.state.item.note !== null && this.state.item.note.length > 0 &&
                                        <Text style={{
                                            marginTop: 16,
                                            marginLeft: 20,
                                            paddingRight: 10,
                                            textAlign: 'left',
                                            fontSize: 15,
                                            width: '70%',
                                            color: 'rgba(0, 0, 0, 0.7)',
                                            fontFamily: 'avenirbook',
                                        }}>{this.state.item.note}</Text>
                                    }
                                    {tag && tag.length > 0 && (
                                        <View style={{
                                            paddingTop: 10,
                                        }}>
                                            {(this.state.item.tag.includes("Travail") || this.state.item.tag.includes("Rdv") || this.state.item.tag.includes("Sport")) && (
                                                <View>
                                                    <View style={{
                                                        marginHorizontal: 20,
                                                        flexDirection: 'row',
                                                        alignItems: 'center'
                                                    }}>
                                                        <Tag color={'#0094FF'} text={"Travail"} tag={this.state.item.tag} />
                                                        <Tag color={'#FF7070'} text={"Rdv"} tag={this.state.item.tag} />
                                                        <Tag color={'#16B476'} text={"Sport"} tag={this.state.item.tag} />
                                                    </View>
                                                    <View style={{ height: 10 }} />
                                                </View>
                                            )}
                                            {(this.state.item.tag.includes("Achat") || this.state.item.tag.includes("Événement") || this.state.item.tag.includes("Autre")) && (
                                                <View style={{
                                                    marginHorizontal: 20,
                                                    flexDirection: 'row',
                                                    alignItems: 'center'
                                                }}>
                                                    <Tag color={'#8235FF'} text={"Achat"} tag={this.state.item.tag} />
                                                    <Tag color={'#FFBE26'} text={"Événement"} tag={this.state.item.tag} />
                                                    <Tag color={'#BABABA'} text={"Autre"} tag={this.state.item.tag} />
                                                </View>
                                            )}
                                        </View>
                                    )}
                                    <TouchableOpacity activeOpacity={100} onPress={() => this.changeLayoutDrop()} style={{ alignItems: 'center', justifyContent: 'center', height: 50, width: '100%' }}>
                                        <Image source={require('../assets/3hdots.png')} style={{ height: 25, width: 25, marginTop: 5 }} resizeMode={"contain"} />
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            )}
                            {this.state.showMoreAdd && (
                                <TouchableOpacity activeOpacity={100} style={{ marginLeft: 20 }} onLongPress={() => null} onPress={() => this.changeLayoutAdd()}>
                                    <Separator />
                                    <TouchableOpacity activeOpacity={100} onPress={() => null} onLongPress={() => null}>
                                        <FlatList data={underTask} keyExtractor={item => item.id.toString()} ListFooterComponent={() => {
                                            return (
                                                <View style={styles.underTaskView}>
                                                    <TextInput placeholder={"+ Ajouter une sous-tâche"} onEndEditing={(e) => this.addNewUnderTask(e)} style={styles.underTaskInput} />
                                                </View>
                                            );
                                        }} renderItem={({ item }) => {
                                            return (
                                                <View style={styles.underTaskView}>
                                                    <Image source={require('../assets/not_archived.png')} style={styles.archivedImg} resizeMode={"contain"} />
                                                    <Text style={[styles.underTaskText, styles.paddingLeft10]}>{item.name}</Text>
                                                </View>
                                            );
                                        }} />
                                    </TouchableOpacity>
                                    <Separator />
                                    <View style={styles.row}>
                                        <TextInput multiline placeholder={"+ Ajouter une note"} onEndEditing={() => this.endEditing()} onChangeText={(e) => this.setState({ item: { ...this.state.item, note: e } })} style={styles.noteInput} value={note} />
                                        {String(name).length > 0 && (
                                            <View style={styles.okView}>
                                                <Text onPress={() => this.changeLayoutAdd()} style={styles.ok}>OK</Text>
                                            </View>
                                        )}
                                    </View>
                                    <Separator />
                                    <View style={styles.bigTagView}>
                                        <View style={styles.tagView}>
                                            <TagAdd onPress={() => this.addTag("Travail")} color={'#0094FF'} text={"Travail"} tag={tag} />
                                            <TagAdd onPress={() => this.addTag("Rdv")} color={'#FF7070'} text={"Rdv"} tag={tag} />
                                            <TagAdd onPress={() => this.addTag("Sport")} color={'#16B476'} text={"Sport"} tag={tag} />
                                        </View>
                                        <View style={styles.space} />
                                        <View style={styles.tagView}>
                                            <TagAdd onPress={() => this.addTag("Achat")} color={'#8235FF'} text={"Achat"} tag={tag} />
                                            <TagAdd onPress={() => this.addTag("Événement")} color={'#FFBE26'} text={"Événement"} tag={tag} />
                                            <TagAdd onPress={() => this.addTag("Autre")} color={'#BABABA'} text={"Autre"} tag={tag} />
                                        </View>
                                    </View>
                                    <TouchableOpacity activeOpacity={100} onPress={() => this.changeLayoutDrop()} style={{ alignItems: 'center', justifyContent: 'center', height: 50, width: '100%', marginTop: -20, marginLeft: -10 }}>
                                        <Image source={require('../assets/3hdots.png')} style={{ height: 25, width: 25, marginTop: 5 }} resizeMode={"contain"} />
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            )}
                            {this.state.showMoreDrop && (
                                <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => null}>
                                    <View style={{ marginHorizontal: 20 }}>
                                        <Separator />
                                        <FlatList scrollEnabled={false} data={underTask} keyExtractor={item => item.id.toString()} ListFooterComponent={() => {
                                            return (
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TextInput numberOfLines={1} placeholder={"+ Ajouter une sous-tâche"} onEndEditing={(e) => this.addNewUnderTask(e)} style={{ color: 'rgba(0, 0, 0, 0.7)', height: 40, fontFamily: 'avenirbook', fontSize: 17, width: '90%' }} />
                                                </View>
                                            );
                                        }} renderItem={({ item, index }) => {
                                            return (
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TextInput numberOfLines={1} placeholder={"+ Ajouter une sous-tâche"} onChange={(e) => this.editUnderTask(e, index)} onEndEditing={(e) => null} style={{ color: 'rgba(0, 0, 0, 0.7)', height: 40, fontFamily: 'avenirbook', fontSize: 17, width: '80%' }} value={item.name} />
                                                    <TouchableOpacity activeOpacity={100} onPress={() => this.removeUnderTask(index)}>
                                                        <Image source={require('../assets/cross.png')} style={{ height: 17, width: 17 }} resizeMode={"contain"} />
                                                    </TouchableOpacity>
                                                </View>
                                            );
                                        }} />
                                        <Separator />
                                        <View style={{ width: '100%' }}>
                                            <TextInput multiline placeholder={"+ Ajouter une note"} onEndEditing={() => this.endEditing()} onChangeText={(e) => this.setState({ item: { ...this.state.item, note: e } })} style={{ minHeight: 45, paddingTop: 14, paddingBottom: 10, width: '80%', textAlign: 'left', fontSize: 17, color: 'rgba(0, 0, 0, 0.7)', fontFamily: 'avenirbook', }} value={note} />
                                        </View>
                                        <Separator />
                                        <View style={styles.bigTagView}>
                                            <View style={styles.tagView}>
                                                <TagAdd onPress={() => this.addTag("Travail")} color={'#0094FF'} text={"Travail"} tag={tag} />
                                                <TagAdd onPress={() => this.addTag("Rdv")} color={'#FF7070'} text={"Rdv"} tag={tag} />
                                                <TagAdd onPress={() => this.addTag("Sport")} color={'#16B476'} text={"Sport"} tag={tag} />
                                            </View>
                                            <View style={styles.space} />
                                            <View style={styles.tagView}>
                                                <TagAdd onPress={() => this.addTag("Achat")} color={'#8235FF'} text={"Achat"} tag={tag} />
                                                <TagAdd onPress={() => this.addTag("Événement")} color={'#FFBE26'} text={"Événement"} tag={tag} />
                                                <TagAdd onPress={() => this.addTag("Autre")} color={'#BABABA'} text={"Autre"} tag={tag} />
                                            </View>
                                        </View>
                                        <Separator />
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontFamily: 'avenirbook', fontSize: 14, color: '#000', marginVertical: 5, paddingTop: 10, width: '90%' }}>Répéter :</Text>
                                            <TouchableOpacity activeOpacity={100} onPress={() => { this.props.update(); this.changeLayoutDrop() }} style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 5, paddingTop: 10 }}>
                                                <Text numberOfLines={1} style={{
                                                    textAlign: 'center',
                                                    fontSize: 18,
                                                    color: '#0094FF',
                                                    fontFamily: 'avenirblack'
                                                }} adjustsFontSizeToFit>OK</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 1 }}>
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('monday', repeat ? !repeat.monday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.monday : false}
                                                        rightText={"Lundi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('tuesday', repeat ? !repeat.tuesday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.tuesday : false}
                                                        rightText={"Mardi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('wednesday', repeat ? !repeat.wednesday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.wednesday : false}
                                                        rightText={"Mercredi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('thursday', repeat ? !repeat.thursday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.thursday : false}
                                                        rightText={"Jeudi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                </View>
                                                <View style={{ flex: 1 }}>
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('friday', repeat ? !repeat.friday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.friday : false}
                                                        rightText={"Vendredi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('saturday', repeat ? !repeat.saturday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.saturday : false}
                                                        rightText={"Samedi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#0094FF'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('sunday', repeat ? !repeat.sunday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.sunday : false}
                                                        rightText={"Dimanche"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                        {this.hasTrue() && (
                                            <TouchableOpacity activeOpacity={100} onPress={() => this.props.deleteBubble(this.state.item.ref)} style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <Image source={require('../assets/blackbin2.png')} style={{ height: 20, width: 20, marginVertical: 10 }} resizeMode={"contain"} />
                                            </TouchableOpacity>
                                        )}
                                        <Separator />
                                        <Text style={{ fontFamily: 'avenirbook', fontSize: 14, color: '#000', marginVertical: 5, paddingTop: 10, width: '90%' }}>Rappels :</Text>
                                        <View>
                                            <View style={{ flexDirection: 'row' }}>
                                                {Platform.OS === 'ios' ? <DateTimePicker
                                                    testID="dateTimePicker"
                                                    mode={'datetime'}
                                                    locale={'fr'}
                                                    is24Hour={true}
                                                    display="default"
                                                    onChange={(event, date) => this.setState({ date, dateActivate: false })}
                                                    value={this.state.date}
                                                    style={{ height: 150, width: (width * 0.6) - 10, marginLeft: 10 }}
                                                /> : this.state.dateActivate ?
                                                        (
                                                            <>
                                                                <DateTimePicker
                                                                    testID="dateTimePicker"
                                                                    mode={'datetime'}
                                                                    locale={'fr'}
                                                                    is24Hour={true}
                                                                    display="default"
                                                                    onChange={(event, date) => this.setState({ date, dateActivate: false })}
                                                                    value={this.state.date}
                                                                    style={{ width: (width * 0.6) - 10, marginLeft: 10 }}
                                                                />
                                                            </>)
                                                        : <Button title="Sélectionner une date" style={{ width: (width * 0.6) - 10, marginLeft: 10 }} onPress={() => this.setState({ dateActivate: true })} />}
                                            </View>
                                            <TouchableOpacity activeOpacity={100} onPress={() => this.addReminder()} style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 5 }}>
                                                <Text numberOfLines={1} style={{
                                                    textAlign: 'center',
                                                    fontSize: 18,
                                                    color: 'rgba(0, 0, 0, 0.7)',
                                                    fontFamily: 'avenirblack'
                                                }} adjustsFontSizeToFit>+</Text>
                                            </TouchableOpacity>
                                            <View style={{
                                                marginLeft: -30,
                                                marginTop: 10,
                                                borderTopWidth: 0.5,
                                                width: width,
                                                borderTopColor: '#E6E4EA',
                                                borderRadius: 30
                                            }} />
                                            {this.state.item.reminders && this.state.item.reminders.length > 0 && (
                                                <View style={{ height: 150, flexDirection: 'row' }}>
                                                    <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                        {this.state.startingIndex !== 0 && <TouchableOpacity activeOpacity={100} onPress={() => this.setState({ startingIndex: this.state.startingIndex - 3 })}><Image source={require('../assets/less.png')} resizeMode={"contain"}
                                                            style={{ height: 20, width: 20, marginRight: 20 }} /></TouchableOpacity>}
                                                    </View>
                                                    <View style={{ width: '60%', flexDirection: 'column' }}>
                                                        {this.state.item.reminders && this.state.item.reminders[this.state.startingIndex] && (
                                                            <View style={{
                                                                height: 50,
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                flexDirection: 'row'
                                                            }}>
                                                                <Text numberOfLines={1} style={{
                                                                    textAlign: 'center',
                                                                    marginTop: 7,
                                                                    fontSize: 20,
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook'
                                                                }}
                                                                    adjustsFontSizeToFit>{moment(new Date(this.state.item.reminders[this.state.startingIndex])).format('lll')}</Text>
                                                                <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(this.state.startingIndex)} style={{ height: 30, width: 30 }}><Image
                                                                    source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                                    style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                            </View>
                                                        )}
                                                        {this.state.item.reminders && this.state.item.reminders[this.state.startingIndex + 1] && (
                                                            <View style={{
                                                                height: 50,
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                flexDirection: 'row'
                                                            }}>
                                                                <Text numberOfLines={1} style={{
                                                                    textAlign: 'center',
                                                                    marginTop: 7,
                                                                    fontSize: 20,
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook'
                                                                }}
                                                                    adjustsFontSizeToFit>{moment(new Date(this.state.item.reminders[this.state.startingIndex + 1])).format('lll')}</Text>
                                                                <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(this.state.startingIndex + 1)} style={{ height: 30, width: 30 }}><Image
                                                                    source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                                    style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                            </View>
                                                        )}
                                                        {this.state.item.reminders && this.state.item.reminders[this.state.startingIndex + 2] && (
                                                            <View style={{
                                                                height: 50,
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                flexDirection: 'row'
                                                            }}>
                                                                <Text numberOfLines={1} style={{
                                                                    textAlign: 'center',
                                                                    marginTop: 7,
                                                                    fontSize: 20,
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook'
                                                                }}
                                                                    adjustsFontSizeToFit>{moment(new Date(this.state.item.reminders[this.state.startingIndex + 2])).format('lll')}</Text>
                                                                <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(this.state.startingIndex + 2)} style={{ height: 30, width: 30 }}><Image
                                                                    source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                                    style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                            </View>
                                                        )}
                                                    </View>
                                                    <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                        {this.state.item.reminders && this.state.startingIndex + 3 < this.state.item.reminders.length && <TouchableOpacity activeOpacity={100} onPress={() => this.setState({ startingIndex: this.state.startingIndex + 3 })}><Image source={require('../assets/more.png')} resizeMode={"contain"}
                                                            style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>}
                                                    </View>
                                                </View>
                                            )}
                                        </View>

                                    </View>
                                </TouchableOpacity>
                            )}
                        </BlurView>
                    </View>

                    {
                        this.state.error && (
                            <Overlay isVisible={true} onBackdropPress={() => this.setState({ error: false })}
                                overlayBackgroundColor={'rgba(0, 0, 0, 0)'} overlayStyle={{
                                    width: width * 0.8,
                                    height: 120,
                                    backgroundColor: '#FFF',
                                    bottom: '5%',
                                    borderRadius: 15,
                                    opacity: 1,
                                }}>
                                <Text numberOfLines={1} style={{
                                    paddingHorizontal: 20,
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: '#000',
                                    fontFamily: 'avenirblack'
                                }} adjustsFontSizeToFit>Désolé... 😅</Text>
                                <Text numberOfLines={2} style={{
                                    paddingHorizontal: 20,
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: '#C4C4C4',
                                    fontFamily: 'avenirbook',
                                    marginTop: 20
                                }}
                                    adjustsFontSizeToFit>{"Il vous sera possible d'archiver cette habitude le " + this.props.date}</Text>
                            </Overlay>
                        )
                    }

                </View >
            );
        } else
            return (<View />);
    }
}

const styles = StyleSheet.create({
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 15,
        elevation: 15
    },
    blurView: {
        width: width * 0.8,
        backgroundColor: 'rgba(255, 255, 255, 0.95)',
        borderRadius: 25,
        borderColor: '#dcdcdc',
    },
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 7,
    },
    cancel: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        fontFamily: 'avenirbook',
        color: '#333'
    },
    cancelView: {
        minHeight: 45,
        width: '40%',
        justifyContent: 'center',
    },
    ok: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        fontFamily: 'avenirblack',
        color: '#0094FF'
    },
    okView: {
        minHeight: 45,
        width: '20%',
        justifyContent: 'center',
    },
    taskInput: {
        minHeight: 45,
        width: '60%',
        textAlign: 'left',
        fontSize: 17,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    underTaskInput: {
        minHeight: 45,
        paddingTop: 3,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    underTaskText: {
        minHeight: 45,
        paddingTop: 16,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    noteInput: {
        minHeight: 45,
        paddingTop: 14,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    row: {
        flexDirection: 'row',
    },
    tagView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    bigTagView: {
        minHeight: 100,
        paddingTop: 10,
    },
    space: {
        height: 10
    },
    underTaskView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    archivedImg: {
        height: 17,
        width: 17,
    },
    paddingLeft10: {
        paddingLeft: 10,
    }
});

function Separator() {
    return (
        <View style={{ width: '60%', height: 1, backgroundColor: 'rgba(0, 0, 0, 0.1)' }} />
    );
}

function Tag(props) {
    if (props.tag.includes(props.text))
        return (
            <View style={{ backgroundColor: props.color, borderRadius: 20, height: 30, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3 }}>
                <Text adjustsFontSizeToFit style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: 'avenirbook', marginTop: 5 }}>{props.text}</Text>
            </View>
        );
    else
        return <View />;
}

function TagAdd(props) {
    return (
        <TouchableOpacity activeOpacity={100} onPress={props.onPress} style={{ backgroundColor: props.color, borderRadius: 20, height: 30, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3 }}>
            <Text adjustsFontSizeToFit style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: props.tag.includes(props.text) ? 'avenirblack' : 'avenirbook', marginTop: 5 }}>{props.text}</Text>
        </TouchableOpacity>
    );
}