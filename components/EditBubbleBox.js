import React from 'react';
import { View, StyleSheet, TextInput, Text, FlatList, Image } from 'react-native';
import { BlurView } from 'expo-blur';
import { width } from '../tools/Layout';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as ExpoHaptics from 'expo-haptics';

export default class EditBubbleBox extends React.Component {
    state = {
        name: null,
        underTask: [],
        note: null,
        tag: [],
    };

    addNewUnderTask(e) {
        const { underTask } = this.state;

        if (e.nativeEvent.text.length == 0)
            return;
        underTask.push({ id: underTask.length, name: e.nativeEvent.text, archived: false });
        this.setState({ underTask });
    }

    addTag(name) {
        const { tag } = this.state;

        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
        if (tag.includes(name))
            tag.splice(tag.indexOf(name), 1);
        else
            tag.push(name);
        this.setState({ tag });
    }

    addBubble() {
        const { name, underTask, note, tag } = this.state;
        if (!name)
            return;
        this.props.onAddBubble({ name, underTask: JSON.stringify(underTask), note, tag: JSON.stringify(tag) });
        this.setState({ name: null, underTask: [], note: null, tag: [] });
        this.props.onCancel();
    }

    render() {
        const { name, underTask, note, tag } = this.state;

        return (
            <View style={styles.container}>
                <BlurView intensity={70} tint={"light"} style={styles.blurView}>
                    <View style={styles.row}>
                        <TextInput autoFocus onSubmitEditing={() => this.addBubble()} placeholder={"+ Ajouter une tâche"} onChangeText={(e) => this.setState({ name: e })} style={styles.taskInput} value={name} />
                        <View style={styles.cancelView}>
                            <Text onPress={this.props.onCancel} style={styles.cancel}>Annuler</Text>
                        </View>
                    </View>
                    <Separator />
                    <View>
                        <FlatList data={underTask} keyExtractor={item => item.id.toString()} ListFooterComponent={() => {
                            return (
                                <View style={styles.underTaskView}>
                                    <TextInput placeholder={"+ Ajouter une sous-tâche"} onEndEditing={(e) => this.addNewUnderTask(e)} style={styles.underTaskInput} />
                                </View>
                            );
                        }} renderItem={({ item }) => {
                            return (
                                <View style={styles.underTaskView}>
                                    <Image source={require('../assets/not_archived.png')} style={styles.archivedImg} resizeMode={"contain"} />
                                    <Text style={[styles.underTaskText, styles.paddingLeft10]}>{item.name}</Text>
                                </View>
                            );
                        }} />
                        {/* <TextInput placeholder={"+ Ajouter une sous-tâche"} style={styles.underTaskInput} value={this.state.underTask[0]} /> */}
                    </View>
                    <Separator />
                    <View style={styles.row}>
                        <TextInput multiline placeholder={"+ Ajouter une note"} onChangeText={(e) => this.setState({ note: e })} style={styles.noteInput} value={note} />
                        {String(name).length > 0 && (
                            <View style={styles.okView}>
                                <Text onPress={() => this.addBubble()} style={styles.ok}>OK</Text>
                            </View>
                        )}
                    </View>
                    <Separator />
                    <View style={styles.bigTagView}>
                        <View style={styles.tagView}>
                            <Tag onPress={() => this.addTag("Travail")} color={'#0094FF'} text={"Travail"} tag={tag} />
                            <Tag onPress={() => this.addTag("Rdv")} color={'#FF7070'} text={"Rdv"} tag={tag} />
                            <Tag onPress={() => this.addTag("Sport")} color={'#16B476'} text={"Sport"} tag={tag} />
                        </View>
                        <View style={styles.space} />
                        <View style={styles.tagView}>
                            <Tag onPress={() => this.addTag("Achat")} color={'#8235FF'} text={"Achat"} tag={tag} />
                            <Tag onPress={() => this.addTag("Événement")} color={'#FFBE26'} text={"Événement"} tag={tag} />
                            <Tag onPress={() => this.addTag("Autre")} color={'#BABABA'} text={"Autre"} tag={tag} />
                        </View>
                    </View>
                </BlurView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: width * 0.8,
        marginLeft: 30,
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.95)',
        borderRadius: 25,
        borderColor: '#dcdcdc',
        marginBottom: 20,
    },
    blurView: {
        width: width * 0.8,
        backgroundColor: 'rgba(255, 255, 255, 0.95)',
        borderRadius: 25,
        borderColor: '#dcdcdc',
    },
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 7,
    },
    cancel: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        fontFamily: 'avenirbook',
        color: '#333'
    },
    cancelView: {
        minHeight: 45,
        width: '40%',
        justifyContent: 'center',
    },
    ok: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        fontFamily: 'avenirblack',
        color: '#0094FF'
    },
    okView: {
        minHeight: 45,
        width: '20%',
        justifyContent: 'center',
    },
    taskInput: {
        minHeight: 45,
        paddingLeft: 20,
        width: '60%',
        textAlign: 'left',
        fontSize: 17,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    underTaskInput: {
        minHeight: 45,
        paddingTop: 3,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    underTaskText: {
        minHeight: 45,
        paddingTop: 16,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    noteInput: {
        minHeight: 45,
        paddingTop: 14,
        paddingLeft: 20,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        color: 'rgba(0, 0, 0, 0.7)',
        fontFamily: 'avenirbook',
    },
    row: {
        flexDirection: 'row',
    },
    tagView: {
        marginHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bigTagView: {
        minHeight: 110,
        paddingTop: 10
    },
    space: {
        height: 10
    },
    underTaskView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
    },
    archivedImg: {
        height: 17,
        width: 17,
    },
    paddingLeft10: {
        paddingLeft: 10,
    }
});

function Separator() {
    return (
        <View style={{ width: '60%', height: 1, backgroundColor: 'rgba(0, 0, 0, 0.1)' }} />
    );
}

function Tag(props) {
    return (
        <TouchableOpacity onPress={props.onPress} style={{ backgroundColor: props.color, borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3 }}>
            <Text adjustsFontSizeToFit style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: props.tag.includes(props.text) ? 'avenirblack' : 'avenirbook', marginTop: 5 }}>{props.text}</Text>
        </TouchableOpacity>
    );
}