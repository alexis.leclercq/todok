import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Image, Text, FlatList } from 'react-native';
import { BlurView } from 'expo-blur';
import { height, width } from '../tools/Layout';
import EditBubbleBox from './EditBubbleBox';
import Bubble from "./Bubble";
import moment from "moment";
import 'moment/locale/fr';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('todok.db');

export default class List extends Component {

    state = {
        addBubble: -1,
    };

    // shouldComponentUpdate(nextProps) {
    //     const { activeIndex } = nextProps;
    //     return (activeIndex >= nextProps.index - 3 && activeIndex <= nextProps.index + 3);
    // }

    isToday(date) {
        return moment().locale('fr').format('L') === date;
    }

    isTomorrow(date) {
        return moment().add("1", "days").locale('fr').format('L') === date;
    }

    isNextTomorrow(date) {
        return moment().add("2", "days").locale('fr').format('L') === date;
    }

    isThisWeek(date) {
        return moment(Date(date)).week() === moment().week();
    }

    isNextWeek(date) {
        return moment(Date(date)).week() === moment().week() + 1;
    }

    hasTrue(repeat) {
        for (let i in repeat) {
            if (repeat[i] === true)
                return true;
        }
        return false;
    }

    getFirstDate(item, day) {
        if (day) {
            if (this.isToday(item.date))
                return "Aujourd'hui";
            else if (this.isTomorrow(item.date))
                return "Demain";
            else if (this.isNextTomorrow(item.date))
                return "Après-demain";
            else
                return day[0].toUpperCase() + day.slice(1);
        }
    }

    getFirstDateWeek(item, day) {
        if (day) {
            if (this.isThisWeek(item.date))
                return "Ma semaine";
            else if (this.isNextWeek(item.date))
                return "Semaine Pro.";
            else
                return "Semaine";
        }
    }

    getSecondDate(item, day, month, type) {
        if (type === 'calendar' && item && item.date && month)
            if (this.isToday(item.date) || this.isTomorrow(item.date) || this.isNextTomorrow(item.date))
                return day[0].toUpperCase() + day.slice(1, 3) + " " + item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
            else
                return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
    }

    getSecondDateWeek(item, day, month, type) {
        if (type === 'calendar' && item && item.date && month)
            return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
    }

    render() {
        let day;
        let month;
        let type = this.props.type;
        if (type === 'calendar' && this.props.item && this.props.item.date) {
            day = moment(new Date(this.props.item.date[3] + this.props.item.date[4] + "/" + this.props.item.date.slice(0, 3) + this.props.item.date.slice(6))).locale('fr').format('dddd');
            month = moment(new Date(this.props.item.date[3] + this.props.item.date[4] + "/" + this.props.item.date.slice(0, 3) + this.props.item.date.slice(6))).locale('fr').format('MMM');
        }
        if (this.props.time === 'day')
            return (
                <View style={{ marginRight: this.props.carouselItems.length === 1 ? width * 0.12 : 0 }}>
                    <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                        <View style={{ width: type === 'perso' ? (width * 0.8) - 80 : 'auto' }}>
                            {type === 'perso' ? (<TextInput style={{
                                textAlign: 'left',
                                fontSize: 22,
                                color: '#fff',
                                fontFamily: 'avenirbook'
                            }} onChangeText={(e) => this.props.editPageName(e)}
                                onEndEditing={() => this.props.editPage(this.props.item, this.props.index)}
                                maxLength={30}
                                placeholderTextColor={"#fff"}
                                placeholder={"+ Ajouter une page"}>{this.props.item.page}</TextInput>)
                                : type === 'calendar' && (
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 22,
                                        color: '#fff',
                                        fontFamily: 'avenirbook'
                                    }}
                                        adjustsFontSizeToFit>{this.getFirstDate(this.props.item, day)}</Text>
                                )}
                        </View>
                        <View style={{ width: type === 'perso' ? 80 : 'auto', alignItems: 'center', paddingLeft: type === 'perso' ? 70 : 0 }}>
                            {type === 'calendar' ? (<Text style={{
                                textAlign: 'left',
                                paddingLeft: 10,
                                fontSize: 14,
                                color: '#fff',
                                fontFamily: 'avenirbook'
                            }}
                                adjustsFontSizeToFit>{this.getSecondDate(this.props.item, day, month, type)}</Text>)
                                : type === 'perso' && (
                                    <TouchableOpacity activeOpacity={100}
                                        onPress={() => this.props.openPageDel()}>
                                        <Image source={require('../assets/plus.png')}
                                            resizeMode={"contain"}
                                            style={{
                                                height: this.props.item.page ? 20 : 0,
                                                width: this.props.item.page ? 20 : 0
                                            }} />
                                    </TouchableOpacity>
                                )}
                        </View>
                    </View>
                    <View style={{
                        height: 1,
                        marginLeft: 30,
                        borderTopWidth: 2,
                        borderTopColor: '#fff',
                        width: width * 0.2,
                        marginBottom: 20
                    }} />
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                        style={{ height: height * 0.8 }}
                        data={this.props.carouselItems[this.props.index].bubbles}
                        extraData={this.props}
                        renderItem={({ item, index }) => {
                            if ((!item.repeat || !this.hasTrue(item.repeat)))
                                if (type === 'calendar' && ((item.date >= moment().locale('fr').subtract(1, "days").format('L') || item.archived !== true)))
                                    return (
                                        <Bubble confettiRef={this.props.confettiRef} archive={this.props.archive} update={() => this.props.updateBubble()} analytics={this.props.analytics} item={item}
                                            deleteBubble={(bubbleRef) => this.props.removeBubble(bubbleRef)} dataBase={db} />
                                    );
                                else if (type !== 'calendar')
                                    return (
                                        <Bubble confettiRef={this.props.confettiRef} archive={this.props.archive} update={() => this.props.updateBubble()} analytics={this.props.analytics} item={item}
                                            deleteBubble={(bubbleRef) => this.props.removeBubble(bubbleRef)} dataBase={db} />
                                    );
                        }}
                        ListFooterComponent={(
                            <View>
                                {this.state.addBubble != this.props.index ? (
                                    <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => this.setState({ addBubble: this.props.index })}
                                        style={{
                                            width: width * 0.8,
                                            marginLeft: 30,
                                            height: 50,
                                            alignItems: 'center',
                                            backgroundColor: 'rgba(255, 255, 255, 0.95)',
                                            borderRadius: 50,
                                            borderColor: '#dcdcdc',
                                            marginBottom: 20,
                                            flexDirection: 'row'
                                        }}>
                                        <BlurView intensity={70} tint={"light"} style={{
                                            width: width * 0.8,
                                            height: 50,
                                            alignItems: 'center',
                                            backgroundColor: 'rgba(255, 255, 255, 0.95)',
                                            borderRadius: 25,
                                            borderColor: '#dcdcdc',
                                            flexDirection: 'row'
                                        }}>
                                            <Text style={{
                                                width: (width * 0.8),
                                                textAlign: 'left',
                                                fontSize: 17,
                                                color: '#rgba(0, 0, 0, 0.4)',
                                                fontFamily: 'avenirbook',
                                                paddingRight: 10,
                                                paddingLeft: 50,
                                                paddingTop: 4
                                            }}>
                                                + Ajouter une tâche
                                                                    </Text>
                                        </BlurView>
                                    </TouchableOpacity>
                                ) : (
                                        <EditBubbleBox onCancel={() => this.setState({ addBubble: -1 })} onAddBubble={(data) => this.props.addBubble(data)} />
                                    )}
                                {this.props.item && this.props.item.repeated && this.props.item.repeated.length > 0 && (
                                    <View>
                                        <View style={{
                                            flexDirection: 'row',
                                            paddingLeft: 30,
                                            alignItems: 'center'
                                        }}>
                                            <Text style={{
                                                textAlign: 'left',
                                                fontSize: 22,
                                                color: '#fff',
                                                fontFamily: 'avenirbook'
                                            }}
                                                adjustsFontSizeToFit>Habitudes</Text>
                                        </View>
                                        <View style={{
                                            height: 1,
                                            marginLeft: 30,
                                            borderTopWidth: 2,
                                            borderTopColor: '#fff',
                                            width: width * 0.2,
                                            marginBottom: 20
                                        }} />
                                        {this.props.item.repeated.map((item2, index2) => {
                                            return <Bubble confettiRef={this.props.confettiRef} archive={this.props.archive} update={() => this.props.updateBubble()} analytics={this.props.analytics} date={item2.date} key={index2}
                                                item={item2.bubble}
                                                deleteBubble={(bubbleRef) => this.props.removeBubble(bubbleRef)} dataBase={db} />;
                                        })}
                                    </View>
                                )}
                            </View>
                        )}
                    />
                </View>
            );
        else if (this.props.time === 'week')
            return (
                <View style={{ marginRight: this.props.carouselItems.length === 1 ? width * 0.12 : 0 }}>
                    <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                        <View style={{ width: type === 'perso' ? (width * 0.8) - 80 : 'auto' }}>
                            {type === 'perso' ? (<TextInput style={{
                                textAlign: 'left',
                                fontSize: 22,
                                color: '#fff',
                                fontFamily: 'avenirbook'
                            }} onChangeText={(e) => this.props.editPageName(e)}
                                onEndEditing={() => this.props.editPage(this.props.item, this.props.index)}
                                maxLength={30}
                                placeholderTextColor={"#fff"}
                                placeholder={"+ Ajouter une page"}>{this.props.item.page}</TextInput>)
                                : type === 'calendar' && (
                                    <Text style={{
                                        textAlign: 'left',
                                        fontSize: 22,
                                        color: '#fff',
                                        fontFamily: 'avenirbook'
                                    }}
                                        adjustsFontSizeToFit>{this.getFirstDateWeek(this.props.item, day)}</Text>
                                )}
                        </View>
                        <View style={{ width: type === 'perso' ? 80 : 'auto', alignItems: 'center', paddingLeft: type === 'perso' ? 70 : 0 }}>
                            {type === 'calendar' ? (<Text style={{
                                textAlign: 'left',
                                paddingLeft: 10,
                                fontSize: 14,
                                color: '#fff',
                                fontFamily: 'avenirbook'
                            }}
                                adjustsFontSizeToFit>{this.getSecondDateWeek(this.props.item, day, month, type)}</Text>)
                                : type === 'perso' && (
                                    <TouchableOpacity activeOpacity={100}
                                        onPress={() => this.props.openPageDel()}>
                                        <Image source={require('../assets/plus.png')}
                                            resizeMode={"contain"}
                                            style={{
                                                height: this.props.item.page ? 20 : 0,
                                                width: this.props.item.page ? 20 : 0
                                            }} />
                                    </TouchableOpacity>
                                )}
                        </View>
                    </View>
                    <View style={{
                        height: 1,
                        marginLeft: 30,
                        borderTopWidth: 2,
                        borderTopColor: '#fff',
                        width: width * 0.2,
                        marginBottom: 20
                    }} />
                </View>
            );
        else if (this.props.time === 'month')
            return (<View />);
        else
            return <View />;
    }
}
