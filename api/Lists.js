import moment from 'moment';
import firebase from '../tools/firebase';
import { AsyncStorage, Alert } from 'react-native';

const firstBubbleName = "Donnez moi un nom";

function findDate(items, date) {
    for (let i in items)
        if (items[i].date === date)
            return i;
    return -1;
}

function isRepeated(repeat) {
    for (let i in repeat) {
        if (repeat[i] === true)
            return true;
    }
    return false;
}

function isDayInRepeated(day, repeated) {
    for (let i in repeated) {
        if (repeated[i].repeat[day.toLowerCase()] === true)
            return true;
    }
    return false;
}

function isAlreadyInList(bubble, page) {
    for (let i in page.bubbles) {
        if (page.bubbles[i] === bubble)
            return true;
    }
    return false;
}

function checkRepeat(carouselItems, time) {
    let repeated = [];
    let day;
    let currentDate = moment().format('L');

    if (time === "day") {
        for (let i in carouselItems) {
            for (let j in carouselItems[i].bubbles) {
                if (carouselItems[i].bubbles[j].repeat && isRepeated(carouselItems[i].bubbles[j].repeat))
                    repeated.push(carouselItems[i].bubbles[j]);
            }
        }
        for (let i in carouselItems) {
            for (let j in repeated) {
                day = moment(new Date(carouselItems[i].date[3] + carouselItems[i].date[4] + "/" + carouselItems[i].date.slice(0, 3) + carouselItems[i].date.slice(6))).locale('en').format('dddd');
                if (isDayInRepeated(day, repeated) && carouselItems[i].date !== currentDate && !isAlreadyInList(repeated[j], carouselItems[i]))
                    carouselItems[i].bubbles.push(repeated[j]);
            }
        }
    } else if (time === "week") {
        for (let i in carouselItems) {
            for (let k in carouselItems[i].days) {
                for (let j in carouselItems[i].days[k].bubbles) {
                    if (carouselItems[i].days[k].bubbles[j].repeat && isRepeated(carouselItems[i].days[k].bubbles[j].repeat))
                        repeated.push(carouselItems[i].days[k].bubbles[j]);
                }
            }
        }
        for (let i in carouselItems) {
            for (let k in carouselItems[i].days) {
                for (let j in repeated) {
                    day = moment(new Date(carouselItems[i].days[k].date[3] + carouselItems[i].days[k].date[4] + "/" + carouselItems[i].days[k].date.slice(0, 3) + carouselItems[i].days[k].date.slice(6))).locale('en').format('dddd');
                    if (isDayInRepeated(day, repeated) && carouselItems[i].days[k].date !== currentDate && !isAlreadyInList(repeated[j], carouselItems[i].days[k]))
                        carouselItems[i].days[k].bubbles.push(repeated[j]);
                }
            }
        }
    } else {

    }
    return carouselItems;
}

function sortBubbles(bubbles, notUsed, activeIndex, time) {
    let currentDate = moment().locale('en').format('L');
    let currentDateFR = moment().locale('fr').format('L');
    let newItems = [];
    notUsed = [];
    let compareDate = -1;
    let max = (activeIndex ? activeIndex + 15 : 28);

    if (time === "day") {
        for (let dayIndex = 0; dayIndex !== max; dayIndex++) {
            let date = moment().add(dayIndex, 'days').format('L');
            newItems.push({ date, bubbles: [] });
        }
        for (let i in bubbles) {
            if (bubbles[i].date && moment(new Date(currentDate).toISOString()).isAfter(moment(new Date(bubbles[i].date[3] + bubbles[i].date[4] + "/" + bubbles[i].date.slice(0, 3) + bubbles[i].date.slice(6)).toISOString()).toISOString())) {
                compareDate = findDate(newItems, currentDateFR);
                if (compareDate === -1)
                    newItems.push({ date: currentDateFR, bubbles: [bubbles[i]] });
                else
                    newItems[compareDate].bubbles.push(bubbles[i]);
            } else {
                compareDate = findDate(newItems, bubbles[i].date ? bubbles[i].date : currentDateFR);
                if (compareDate !== -1)
                    newItems[compareDate].bubbles.push(bubbles[i]);
                else
                    notUsed.push(bubbles[i]);
            }
        }
    } else if (time === "week") {
        for (let weekIndex = 0; weekIndex !== 4; weekIndex++) {
            newItems.push({ days: [], date_start: moment().add(weekIndex * 7, 'days').format('L'), date_end: moment().add(weekIndex * 7 + 7, 'days').format('L') });
            for (let dayIndex = 0; dayIndex !== 7; dayIndex++) {
                let date = moment().add((weekIndex * 7) + dayIndex, 'days').format('L');
                newItems[weekIndex].days.push({ date, bubbles: [] });
            }
            for (let i in bubbles) {
                if (bubbles[i].date && moment(new Date(currentDate).toISOString()).isAfter(moment(new Date(bubbles[i].date[3] + bubbles[i].date[4] + "/" + bubbles[i].date.slice(0, 3) + bubbles[i].date.slice(6)).toISOString()).toISOString())) {
                    compareDate = findDate(newItems[weekIndex].days, currentDateFR);
                    if (compareDate !== -1)
                        newItems[weekIndex].days[compareDate].bubbles.push(bubbles[i]);
                } else {
                    compareDate = findDate(newItems[weekIndex].days, bubbles[i].date ? bubbles[i].date : currentDateFR);
                    if (compareDate !== -1)
                        newItems[weekIndex].days[compareDate].bubbles.push(bubbles[i]);
                    else
                        notUsed.push(bubbles[i]);
                }
            }
        }
    } else {

    }
    newItems = checkRepeat(newItems, time);
    return newItems;
}

function getNotUsed(bubbles, notUsed, activeIndex) {
    let currentDate = moment().locale('en').format('L');
    let currentDateFR = moment().locale('fr').format('L');
    let newItems = [];
    notUsed = [];
    let compareDate = -1;
    let max = (activeIndex ? activeIndex + 15 : 28);

    for (let dayIndex = 0; dayIndex !== max; dayIndex++) {
        let date = moment().add(dayIndex, 'days').format('L');
        newItems.push({ date, bubbles: [] });
    }
    for (let i in bubbles) {
        if (bubbles[i].date && moment(new Date(currentDate).toISOString()).isAfter(moment(new Date(bubbles[i].date[3] + bubbles[i].date[4] + "/" + bubbles[i].date.slice(0, 3) + bubbles[i].date.slice(6)).toISOString()).toISOString())) {
            compareDate = findDate(newItems, currentDateFR);
            if (compareDate === -1)
                newItems.push({ date: currentDateFR, bubbles: [bubbles[i]] });
            else
                newItems[compareDate].bubbles.push(bubbles[i]);
        } else {
            compareDate = findDate(newItems, bubbles[i].date ? bubbles[i].date : currentDateFR);
            if (compareDate !== -1)
                newItems[compareDate].bubbles.push(bubbles[i]);
            else
                notUsed.push(bubbles[i]);
        }
    }
    return notUsed;
}

function hasTrue(repeat) {
    for (let i in repeat) {
        if (repeat[i] === true)
            return true;
    }
    return false;
}

function isDayInRepeatedReal(day, repeated) {
    if (repeated.repeat[day.toLowerCase()] === true)
        return true;
    return false;
}

function getRepeated(carouselItems) {
    let day;
    let repeated = [];

    for (let i in carouselItems) {
        for (let j in carouselItems[i].bubbles) {
            if (carouselItems[i].bubbles[j].repeat && isRepeated(carouselItems[i].bubbles[j].repeat))
                repeated.push(carouselItems[i].bubbles[j]);
        }
    }

    for (let i in carouselItems) {
        carouselItems[i].repeated = [];
        for (let j in carouselItems[i].bubbles) {
            day = moment(new Date(carouselItems[i].date[3] + carouselItems[i].date[4] + "/" + carouselItems[i].date.slice(0, 3) + carouselItems[i].date.slice(6))).locale('en').format('dddd');
            if (carouselItems[i].bubbles[j].repeat && carouselItems[i].bubbles[j].repeat !== undefined && hasTrue(carouselItems[i].bubbles[j].repeat) && isDayInRepeatedReal(day, carouselItems[i].bubbles[j])) {
                carouselItems[i].repeated.push({ bubble: carouselItems[i].bubbles[j], date: carouselItems[i].date });
            }
        }
    }

    return carouselItems;
}

function newSortBubblesPerso(pages) {
    let newItems = [];
    for (let i in pages) {
        newItems.push({
            page: pages[i].name,
            bubbles: pages[i].bubbles,
        });
    }
    newItems.push({ page: "", bubbles: [] });
    return newItems;
}

export async function getListsLocalData(db, time, hasInternet, list, lists, user, state) {
    let res = { carouselItems: null, bubbles: null, pages: null, notUsed: null };

    await new Promise((resolve, reject) => {
        db.transaction(
            tx => {
                tx.executeSql(
                    "create table if not exists users (archive BOOLEAN DEFAULT NULL, last_updated text, lists text, background text);"
                );
                tx.executeSql(
                    "create table if not exists lists (id text, name text, type text, pages text, bubbles text);"
                );
                tx.executeSql(
                    "create table if not exists bubbles (id text, archived text, date text, days text, name text, reminders text, repeat text, note text, tag text, underTask text, open text);"
                );
                tx.executeSql(
                    "create table if not exists pages (id text, name text, bubbles text);"
                );
            }, null);
        if (list.type == "calendar") {
            db.transaction(
                tx => {
                    tx.executeSql("select * from lists where id = (?)", [list.ref], async (_, { rows }) => {
                        if (rows.length > 0) {
                            let localBubbles = JSON.parse(rows.item(0).bubbles);
                            let command = "";
                            command += "id = '" + localBubbles[0] + "'";
                            for (let i = 1; localBubbles[i]; i++)
                                if (localBubbles[i] != "")
                                    command += " or id = '" + localBubbles[i] + "'";
                            let bubbles = [];
                            tx.executeSql("select * from bubbles where " + command, [], async (_, { rows }) => {
                                for (let i = 0; i != rows.length; i++) {
                                    let p = rows.item(i);
                                    bubbles.push({
                                        id: i,
                                        name: p.name,
                                        date: p.date,
                                        archived: p.archived ? JSON.parse(p.archived) : null,
                                        days: p.days,
                                        ref: p.id,
                                        reminders: p.reminders ? JSON.parse(p.reminders) : null,
                                        repeat: p.repeat ? JSON.parse(p.repeat) : null,
                                        note: p.note,
                                        tag: p.tag ? JSON.parse(p.tag) : null,
                                        underTask: p.underTask ? JSON.parse(p.underTask) : null,
                                        open: p.open ? p.open : "false"
                                    });
                                }
                                if (await AsyncStorage.getItem('todokDBNeedUpdate' + list.ref) && hasInternet) {
                                    await AsyncStorage.removeItem('todokDBNeedUpdate' + list.ref);
                                    let localBubblesUpdate = [];
                                    for (let i in bubbles) {
                                        localBubblesUpdate.push(bubbles[i].ref);
                                        firebase.firestore().collection("/bubbles").doc(bubbles[i].ref).set({
                                            name: bubbles[i].name,
                                            date: bubbles[i].date,
                                            archived: bubbles[i].archived,
                                            days: bubbles[i].days,
                                            reminders: bubbles[i].reminders,
                                            repeat: bubbles[i].repeat,
                                            note: bubbles[i].note,
                                            tag: bubbles[i].tag,
                                            underTask: bubbles[i].underTask,
                                            open: bubbles[i].open,
                                        });
                                    }
                                    firebase.firestore().collection("/lists").doc(list.ref).set({
                                        bubbles: localBubblesUpdate,
                                        name: list.name,
                                        type: 'calendar'
                                    });
                                    let localListsUpdate = [];
                                    for (let i in lists)
                                        localListsUpdate.push(lists[i].ref);
                                    firebase.firestore().collection("/users").doc(user.uid).update({
                                        lists: localListsUpdate
                                    });
                                }
                                let carouselItems = sortBubbles(bubbles, state.notUsed, state.activeIndex, time);
                                let notUsed = getNotUsed(bubbles, state.notUsed, state.activeIndex);
                                resolve({ bubbles, carouselItems, notUsed })
                            });
                        } else {
                            reject("No data for local calendar.");
                        }
                    });
                },
                null,
            );
        } else {
            db.transaction(
                tx => {
                    tx.executeSql("select * from lists where id = (?)", [list.ref], async (_, { rows }) => {
                        if (rows.length > 0) {
                            let localPages = JSON.parse(rows.item(0).pages);
                            let commandPages = "";
                            commandPages += "id = '" + localPages[0] + "'";
                            for (let i = 1; localPages[i]; i++)
                                if (localPages[i] != "")
                                    commandPages += " or id = '" + localPages[i] + "'";
                            let pages = [];
                            tx.executeSql("select * from pages where " + commandPages, [], async (_, { rows }) => {
                                for (let i = 0; i != rows.length; i++) {
                                    let p = rows.item(i);
                                    let localBubbles = JSON.parse(p.bubbles);
                                    let commandBubbles = "";
                                    if (localBubbles[0])
                                        commandBubbles += "id = '" + localBubbles[0] + "'";
                                    for (let i = 1; localBubbles[i]; i++)
                                        if (localBubbles[i] != "")
                                            commandBubbles += " or id = '" + localBubbles[i] + "'";
                                    let bubbles = [];
                                    await db.transaction(
                                        async tx => {
                                            tx.executeSql("select * from bubbles where " + commandBubbles, [], async (_, { rows }) => {
                                                for (let i = 0; i != rows.length; i++) {
                                                    let p = rows.item(i);
                                                    bubbles.push({
                                                        id: i,
                                                        name: p.name,
                                                        date: p.date,
                                                        archived: p.archived ? JSON.parse(p.archived) : null,
                                                        days: p.days,
                                                        ref: p.id,
                                                        reminders: p.reminders ? JSON.parse(p.reminders) : null,
                                                        repeat: p.repeat ? JSON.parse(p.repeat) : null,
                                                        note: p.note,
                                                        tag: p.tag ? JSON.parse(p.tag) : null,
                                                        underTask: p.underTask ? JSON.parse(p.underTask) : null,
                                                        open: p.open ? p.open : "false"
                                                    });
                                                    if (await AsyncStorage.getItem('todokDBNeedUpdate' + p.id) && hasInternet) {
                                                        console.log("Upload bubble with id: " + p.id);
                                                        await AsyncStorage.removeItem('todokDBNeedUpdate' + p.id)
                                                        firebase.firestore().collection("/bubbles").doc(p.id).set({
                                                            name: p.name,
                                                            date: p.date,
                                                            archived: p.archived,
                                                            days: p.days,
                                                            reminders: p.reminders,
                                                            repeat: p.repeat,
                                                            note: p.note,
                                                            tag: p.tag,
                                                            underTask: p.underTask,
                                                            open: p.open
                                                        });
                                                    }
                                                }
                                            });
                                        });
                                    pages.push({
                                        id: i,
                                        name: p.name,
                                        ref: p.id,
                                        bubbles: bubbles,
                                    });
                                    if (await AsyncStorage.getItem('todokDBNeedUpdate' + p.id) && hasInternet) {
                                        await AsyncStorage.removeItem('todokDBNeedUpdate' + p.id)
                                        console.log("Upload page with id: " + p.id);
                                        firebase.firestore().collection("/pages").doc(p.id).set({
                                            name: p.name,
                                            bubbles: localBubbles
                                        });
                                    }
                                }
                                if (await AsyncStorage.getItem('todokDBNeedUpdate' + list.ref) && hasInternet) {
                                    await AsyncStorage.removeItem('todokDBNeedUpdate' + list.ref);
                                    console.log("Need upload local data")
                                    firebase.firestore().collection("/lists").doc(list.ref).set({
                                        pages: localPages,
                                        name: list.name,
                                        type: 'perso'
                                    });
                                    let localListsUpdate = [];
                                    for (let i in lists)
                                        localListsUpdate.push(lists[i].ref);
                                    firebase.firestore().collection("/users").doc(user.uid).update({
                                        lists: localListsUpdate
                                    });
                                }
                                let carouselItems = newSortBubblesPerso(pages);
                                setTimeout(() => {
                                    resolve({ pages, carouselItems });
                                }, 100)
                            });
                        } else {
                            reject("No data for local personalised.");
                        }
                    });
                },
                null,
            );
        }
    }).then((value) => {
        res = value;
    });
    return res;
}

export async function getListsOnlineData(db, time, lists, selected, state) {
    let res = { carouselItems: null, bubbles: null, pages: null, notUsed: null };

    db.transaction(
        tx => {
            // tx.executeSql("drop table if exists bubbles");
            // tx.executeSql("drop table if exists pages");
            tx.executeSql(
                "create table if not exists bubbles (id text, archived text, date text, days text, name text, reminders text, repeat text, note text, tag text, underTask text, open text);"
            );
            tx.executeSql(
                "create table if not exists pages (id text, name text, bubbles text);"
            );
        },
        null,
    );
    if (lists[selected].ref && lists[selected].type === 'calendar') {
        await firebase.firestore().collection('lists')
            .doc(lists[selected].ref)
            .get()
            .then(async (docRef) => {
                if (docRef.exists) {
                    if (docRef.data().bubbles) {
                        let bubbles = [];
                        let localBubbles = [];
                        for (let i in docRef.data().bubbles) {
                            let p = await firebase.firestore().collection("bubbles").doc(docRef.data().bubbles[i])
                                .get()
                                .then((data) => data)
                                .catch((error) => {
                                    console.log(error)
                                    Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊');
                                })
                            bubbles.push({
                                id: i,
                                name: p.data().name ? p.data().name : null,
                                date: p.data().date ? p.data().date : null,
                                archived: p.data().archived ? p.data().archived : null,
                                days: p.data().days ? p.data().days : 0,
                                ref: p.ref.id ? p.ref.id : null,
                                reminders: p.data().reminders ? p.data().reminders : null,
                                repeat: p.data().repeat ? p.data().repeat : null,
                                note: p.data().note ? p.data().note : "",
                                tag: p.data().tag ? p.data().tag : null,
                                underTask: p.data().underTask ? p.data().underTask : null,
                                open: "false",
                            });
                            db.transaction(
                                tx => {
                                    tx.executeSql("select * from bubbles where id = (?)", [p.id], async (_, { rows }) => {
                                        if (rows.length == 0) {
                                            tx.executeSql("insert into bubbles (id, archived, date, days, name, reminders, repeat, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
                                                p.id,
                                                p.data().archived ? JSON.stringify(p.data().archived) : null,
                                                p.data().date ? p.data().date : "",
                                                p.data().days ? p.data().days : 0,
                                                p.data().name ? p.data().name : "",
                                                p.data().reminders ? JSON.stringify(p.data().reminders) : null,
                                                p.data().repeat ? JSON.stringify(p.data().repeat) : null,
                                                p.data().note ? p.data().note : "",
                                                p.data().tag ? JSON.stringify(p.data().tag) : null,
                                                p.data().underTask ? JSON.stringify(p.data().underTask) : null,
                                                p.data().open ? p.data().open : "false",
                                            ]);
                                        } else {
                                            tx.executeSql("update bubbles set archived = (?), date = (?), days = (?), name = (?), reminders = (?), repeat = (?), note = (?), tag = (?), underTask = (?), open = (?) where id = (?)", [
                                                p.data().archived ? JSON.stringify(p.data().archived) : null,
                                                p.data().date ? p.data().date : "",
                                                p.data().days ? p.data().days : 0,
                                                p.data().name ? p.data().name : "",
                                                p.data().reminders ? JSON.stringify(p.data().reminders) : null,
                                                p.data().repeat ? JSON.stringify(p.data().repeat) : null,
                                                p.data().note ? p.data().note : "",
                                                p.data().tag ? JSON.stringify(p.data().tag) : null,
                                                p.data().underTask ? JSON.stringify(p.data().underTask) : null,
                                                p.data().open ? p.data().open : "false",
                                                p.id,
                                            ]);
                                        }
                                    });
                                },
                                null,
                            );
                            localBubbles.push(p.id);
                        }
                        db.transaction(
                            tx => {
                                tx.executeSql("update lists set bubbles = (?) where id = (?)", [JSON.stringify(localBubbles), lists[selected].ref]);
                            },
                            null,
                        );
                        let carouselItems = sortBubbles(bubbles, state.notUsed, state.activeIndex, time);
                        let notUsed = getNotUsed(bubbles, state.notUsed, state.activeIndex);
                        res = { ...res, bubbles, carouselItems, notUsed };
                    } else {
                        res = {
                            ...res,
                            bubbles: [],
                            carouselItems: sortBubbles([], state.notUsed, state.activeIndex, time),
                            notUsed: getNotUsed([], state.notUsed, state.activeIndex),
                        }
                    }
                } else {
                    res = {
                        ...res,
                        bubbles: [],
                        carouselItems: sortBubbles([], state.notUsed, state.activeIndex, time),
                        notUsed: getNotUsed([], state.notUsed, state.activeIndex),
                    }
                }
            })
            .catch((err) => {
                console.log(err)
                Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
            });
        getRepeated(res.carouselItems);
    } else {
        await firebase.firestore().collection('lists')
            .doc(lists[selected].ref)
            .get()
            .then(async (docRef) => {
                if (docRef.exists) {
                    if (docRef.data().pages) {
                        let pages = [];
                        let localPages = [];
                        for (let i in docRef.data().pages) {
                            let p = await firebase.firestore().collection("pages").doc(docRef.data().pages[i])
                                .get()
                                .then((data) => data)
                                .catch((error) => {
                                    console.log(error)
                                    Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                                })
                            let bubbles = [];
                            let localBubbles = [];
                            for (let j in p.data().bubbles) {
                                let b = await firebase.firestore().collection("bubbles").doc(p.data().bubbles[j])
                                    .get()
                                    .then((data) => data)
                                    .catch((error) => {
                                        console.log(error)
                                        Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                                    })
                                bubbles.push({
                                    id: j,
                                    name: b.data().name,
                                    archived: b.data().archived,
                                    ref: b.ref.id,
                                    reminders: b.data().reminders,
                                    note: b.data().note,
                                    tag: b.data().tag,
                                    underTask: b.data().underTask,
                                    open: b.data().open,
                                });
                                db.transaction(
                                    tx => {
                                        tx.executeSql("select * from bubbles where id = (?)", [b.ref.id], async (_, { rows }) => {
                                            if (rows.length == 0) {
                                                tx.executeSql("insert into bubbles (id, archived, date, days, name, reminders, repeat, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
                                                    b.id,
                                                    b.data().archived ? JSON.stringify(b.data().archived) : null,
                                                    "",
                                                    0,
                                                    b.data().name ? b.data().name : "",
                                                    b.data().reminders ? JSON.stringify(b.data().reminders) : null,
                                                    null,
                                                    b.data().note ? b.data().note : "",
                                                    b.data().tag ? JSON.stringify(b.data().tag) : null,
                                                    b.data().underTask ? JSON.stringify(b.data().underTask) : null,
                                                    b.data().open ? b.data().open : "false",
                                                ]);
                                            } else {
                                                tx.executeSql("update bubbles set archived = (?), date = (?), days = (?), name = (?), reminders = (?), repeat = (?), note = (?), tag = (?), underTask = (?), open = (?) where id = (?)", [
                                                    b.data().archived ? JSON.stringify(b.data().archived) : null,
                                                    "",
                                                    0,
                                                    b.data().name ? b.data().name : "",
                                                    b.data().reminders ? JSON.stringify(b.data().reminders) : null,
                                                    null,
                                                    b.data().note ? b.data().note : "",
                                                    b.data().tag ? JSON.stringify(b.data().tag) : null,
                                                    b.data().underTask ? JSON.stringify(b.data().underTask) : null,
                                                    b.data().open ? b.data().open : "false",
                                                    b.ref.id,
                                                ]);
                                            }
                                        });
                                    },
                                    null,
                                );
                                localBubbles.push(b.id);
                            }
                            pages.push({
                                id: i,
                                name: p.data().name,
                                bubbles: bubbles,
                                ref: p.ref.id
                            });
                            let localBubblesList = [];
                            for (let i in bubbles)
                                localBubblesList.push(bubbles[i].ref);
                            db.transaction(
                                tx => {
                                    tx.executeSql("select * from pages where id = (?)", [p.id], async (_, { rows }) => {
                                        if (rows.length == 0) {
                                            tx.executeSql("insert into pages (id, name, bubbles) values (?, ?, ?)", [
                                                p.id,
                                                p.data().name,
                                                JSON.stringify(localBubblesList)
                                            ]);
                                        } else {
                                            tx.executeSql("update pages set name = (?), bubbles = (?) where id = (?)", [
                                                p.data().name,
                                                JSON.stringify(localBubblesList),
                                                p.id,
                                            ]);
                                        }
                                    });
                                },
                                null,
                            );
                            localPages.push(p.id);
                        }
                        db.transaction(
                            tx => {
                                tx.executeSql("update lists set pages = (?) where id = (?)", [JSON.stringify(localPages), lists[selected].ref]);
                            },
                            null,
                        );
                        let carouselItems = newSortBubblesPerso(pages);
                        res = { ...res, pages, carouselItems };
                    } else {
                        await firebase.firestore().collection('pages').add({
                            name: firstBubbleName,
                            bubbles: [],
                        }).then(async (docRef) => {
                            db.transaction(
                                tx => {
                                    tx.executeSql("insert into pages (id, name, bubbles) values (?, ?, ?)", [
                                        docRef.id,
                                        firstBubbleName,
                                        JSON.stringify([]),
                                    ]);
                                    tx.executeSql("update lists set pages = (?) where id = (?)", [JSON.stringify([docRef.id]), lists[selected].ref]);
                                },
                                null,
                            );
                            await firebase.firestore().collection('lists').doc(lists[selected].ref).update({ pages: [docRef.id] }).then(() => res = {
                                ...res,
                                pages: [{
                                    id: 0,
                                    name: firstBubbleName,
                                    bubbles: [],
                                    ref: docRef.id,
                                }],
                                carouselItems: newSortBubblesPerso([{
                                    id: 0,
                                    name: firstBubbleName,
                                    bubbles: [],
                                    ref: docRef.id,
                                }]),
                            });
                        })
                            .catch((error) => {
                                console.log(error)
                                Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                            })
                    }
                } else {
                    await firebase.firestore().collection('pages').add({
                        name: firstBubbleName,
                        bubbles: [],
                    }).then(async (docRef) => {
                        db.transaction(
                            tx => {
                                tx.executeSql("insert into pages (id, name, bubbles) values (?, ?, ?)", [
                                    docRef.id,
                                    firstBubbleName,
                                    JSON.stringify([]),
                                ]);
                                tx.executeSql("update lists set pages = (?) where id = (?)", [JSON.stringify([docRef.id]), lists[selected].ref]);
                            },
                            null,
                        );
                        await firebase.firestore().collection('lists').doc(lists[selected].ref).set({ pages: [docRef.id] }).then(() => res = {
                            ...res,
                            pages: [{
                                id: 0,
                                name: firstBubbleName,
                                bubbles: [],
                                ref: docRef.id,
                            }],
                            carouselItems: newSortBubblesPerso([{
                                id: 0,
                                name: firstBubbleName,
                                bubbles: [],
                                ref: docRef.id,
                            }]),
                        });
                    })
                        .catch((error) => {
                            console.log(error)
                            Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                        })
                }
            })
            .catch((error) => {
                console.log(error)
                Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
            })
    }
    return res;
}

export async function removeBubbleFromList(time, state, props, id, db) {
    let newId = 0;
    let res = {};

    if (props.lists && props.lists[props.selected] && props.lists[props.selected].type === 'calendar') {
        let newList = [];
        let newListRef = [];
        db.transaction(
            tx => {
                tx.executeSql("delete from bubbles where id = (?)", [id]);
                tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
            },
            null,
        );
        for (let i in state.bubbles) {
            if (state.bubbles[i].ref !== id) {
                newList.push({ id: newId, ...state.bubbles[i] });
                newListRef.push(state.bubbles[i].ref);
                newId++;
            }
        }
        res = { ...res, bubbles: newList };
        AsyncStorage.setItem("todokDBNeedUpdate" + props.lists[props.selected].ref, "true");
        db.transaction(
            tx => {
                tx.executeSql("update lists set bubbles = (?) where id = (?)", [JSON.stringify(newListRef), props.lists[props.selected].ref]);
            },
            null,
        );
        let carouselItems = sortBubbles(newList, state.notUsed, state.activeIndex, time);
        let notUsed = getNotUsed(newList, state.notUsed, state.activeIndex);
        res = { ...res, carouselItems: getRepeated(carouselItems), notUsed };
    } else {
        db.transaction(
            tx => {
                tx.executeSql("delete from bubbles where id = (?)", [id]);
                tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
            },
            null,
        );
        let newBubbles = [];
        for (let i in state.pages[state.activeIndex].bubbles) {
            if (state.pages[state.activeIndex].bubbles[i].ref !== id) {
                newBubbles.push({ ...state.pages[state.activeIndex].bubbles[i], id: newId });
                newId++;
            }
        }
        state.pages[state.activeIndex].bubbles = newBubbles;
        let localBubbles = [];
        for (let i in newBubbles)
            localBubbles.push(newBubbles[i].ref)
        AsyncStorage.setItem("todokDBNeedUpdate" + state.pages[state.activeIndex].ref, "true");
        db.transaction(
            tx => {
                tx.executeSql("update pages set bubbles = (?) where id = (?)", [JSON.stringify(localBubbles), state.pages[state.activeIndex].ref]);
            },
            null,
        );
        let carouselItems = newSortBubblesPerso(state.pages);
        res = { ...res, carouselItems };
    }
    return res;
}

export async function addBubbleToList(time, state, props, type, db, user, data) {
    let res = {};

    const id1 = Math.random().toString(36).substring(3);
    const id2 = Math.random().toString(36).substring(3);
    const ref = 'local' + user.uid + 'Todok' + id1 + id2;
    // console.log("-------------")
    // console.log("Date: " + state.carouselItems[state.activeIndex].date)
    // console.log("ActiveIndex: " + state.activeIndex)
    // console.log("-------------")
    await new Promise((resolve, reject) => {
        if (type === 'calendar') {
            db.transaction(
                tx => {
                    tx.executeSql("select * from bubbles where id = (?)", [ref], async (_, { rows }) => {
                        if (rows.length == 0) {
                            tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                            tx.executeSql("insert into bubbles (id, name, note, tag, underTask, date) values (?, ?, ?, ?, ?, ?)", [
                                ref,
                                data.name,
                                data.note,
                                data.tag,
                                data.underTask,
                                state.carouselItems[state.activeIndex].date,
                            ]);
                            state.bubbles.push({
                                id: state.bubbles.length,
                                name: data.name,
                                note: data.note,
                                tag: JSON.parse(data.tag),
                                underTask: JSON.parse(data.underTask),
                                date: state.carouselItems[state.activeIndex].date,
                                ref: ref,
                            });
                            let localBubbles = [];
                            for (let i in state.bubbles)
                                localBubbles.push(state.bubbles[i].ref);
                            tx.executeSql("update lists set bubbles = (?) where id = (?)", [JSON.stringify(localBubbles), props.lists[props.selected].ref]);
                            let carouselItems = sortBubbles(state.bubbles, state.notUsed, state.activeIndex, time);
                            let notUsed = getNotUsed(state.bubbles, state.notUsed, state.activeIndex);
                            resolve({
                                bubbles: state.bubbles,
                                notUsed,
                                carouselItems: getRepeated(carouselItems),
                            });
                            AsyncStorage.setItem("todokDBNeedUpdate" + ref, "true");
                            AsyncStorage.setItem("todokDBNeedUpdate" + props.lists[props.selected].ref, "true");
                        } else {
                            resolve({ bubbles: state.bubbles, notUsed: state.notUsed, carouselItems: state.carouselItems });
                        }
                    });
                }
            );
        } else {
            if (!state.carouselItems[state.activeIndex].page) {
                const id3 = Math.random().toString(36).substring(3);
                const id4 = Math.random().toString(36).substring(3);
                const ref2 = 'local' + user.uid + 'Todok' + id3 + id4;
                // AJOUT D'UNE BULLE DANS UNE PAGE NON EXISTANTE
                db.transaction(
                    tx => {
                        tx.executeSql("select * from bubbles where id = (?)", [ref], async (_, { rows }) => {
                            if (rows.length == 0) {
                                tx.executeSql("select * from pages where id = (?)", [ref2], async (_, { rows }) => {
                                    if (rows.length == 0) {
                                        tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                                        tx.executeSql("insert into pages (id, name, bubbles) values (?, ?, ?)", [
                                            ref2,
                                            "+ Ajouter une page",
                                            JSON.stringify([ref])
                                        ]);
                                        tx.executeSql("insert into bubbles (id, name, note, tag, underTask) values (?, ?, ?, ?, ?)", [
                                            ref,
                                            data.name,
                                            data.note,
                                            data.tag,
                                            data.underTask,
                                        ]);
                                        let len = state.pages.length;
                                        state.pages.push({
                                            id: len,
                                            bubbles: [{
                                                id: 0, name: data.name, note: data.note, tag: JSON.parse(data.tag), underTask: JSON.parse(data.underTask), ref: ref
                                            }],
                                            name: "+ Ajouter une page",
                                            ref: ref2
                                        });
                                        let localPages = [];
                                        for (let i in state.pages)
                                            localPages.push(state.pages[i].ref)
                                        tx.executeSql("update lists set pages = (?) where id = (?)", [JSON.stringify(localPages), props.lists[props.selected].ref]);
                                    }
                                    let carouselItems = newSortBubblesPerso(state.pages);
                                    resolve({
                                        pages: state.pages,
                                        carouselItems,
                                        newBubble: undefined,
                                    });
                                    AsyncStorage.setItem("todokDBNeedUpdate" + ref, "true");
                                    AsyncStorage.setItem("todokDBNeedUpdate" + props.lists[props.selected].ref, "true");
                                });
                            } else {
                                resolve({ pages: state.pages, carouselItems: state.carouselItems, newBubble: undefined });
                            }
                        });
                    }
                );
            } else {
                // AJOUT D'UNE BULLE DANS UNE PAGE EXISTANTE
                db.transaction(
                    tx => {
                        tx.executeSql("select * from bubbles where id = (?)", [ref], async (_, { rows }) => {
                            if (rows.length == 0) {
                                tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                                tx.executeSql("insert into bubbles (id, name, note, tag, underTask) values (?, ?, ?, ?, ?)", [
                                    ref,
                                    data.name,
                                    data.note,
                                    data.tag,
                                    data.underTask,
                                ]);
                                state.pages[state.activeIndex].bubbles.push({
                                    id: state.pages[state.activeIndex].bubbles.length,
                                    name: data.name,
                                    note: data.note,
                                    tag: JSON.parse(data.tag),
                                    underTask: JSON.parse(data.underTask),
                                    ref: ref
                                });
                                let localBubbles = [];
                                for (let i in state.pages[state.activeIndex].bubbles)
                                    localBubbles.push(state.pages[state.activeIndex].bubbles[i].ref)
                                tx.executeSql("update pages set bubbles = (?) where id = (?)", [JSON.stringify(localBubbles), state.pages[state.activeIndex].ref]);
                                let carouselItems = newSortBubblesPerso(state.pages);
                                resolve({
                                    pages: state.pages,
                                    carouselItems,
                                    newBubble: undefined,
                                });
                                AsyncStorage.setItem("todokDBNeedUpdate" + ref, "true");
                                AsyncStorage.setItem("todokDBNeedUpdate" + state.pages[state.activeIndex].ref, "true");
                            } else {
                                resolve({ pages: state.pages, carouselItems: state.carouselItems, newBubble: undefined });
                            }
                        });
                    }
                );
            }
        }
    }).then((value) => {
        res = value;
    });
    return res;
}