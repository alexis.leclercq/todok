import { AsyncStorage } from 'react-native';
import { removeBubbleFromList } from './Lists';

function newSortBubblesPerso(pages) {
    let newItems = [];
    for (let i in pages) {
        newItems.push({
            page: pages[i].name,
            bubbles: pages[i].bubbles,
        });
    }
    newItems.push({ page: "", bubbles: [] });
    return newItems;
}

export async function addPage(state, props, user, db) {
    let res = {};

    await new Promise((resolve, reject) => {
        const id1 = Math.random().toString(36).substring(3);
        const id2 = Math.random().toString(36).substring(3);
        const ref = 'local' + user.uid + 'Todok' + id1 + id2;
        db.transaction(
            tx => {
                tx.executeSql("select * from pages where id = (?)", [ref], async (_, { rows }) => {
                    if (rows.length == 0) {
                        tx.executeSql("insert into pages (id, name, bubbles) values (?, ?, ?)", [
                            ref,
                            state.newPage,
                            JSON.stringify([]),
                        ]);
                        state.pages.push({
                            id: state.pages.length,
                            name: state.newPage,
                            ref: ref,
                            bubbles: [],
                        });
                        let localPages = [];
                        for (let i in state.pages)
                            localPages.push(state.pages[i].ref);
                        tx.executeSql("update lists set pages = (?) where id = (?)", [JSON.stringify(localPages), props.lists[props.selected].ref]);
                        let carouselItems = newSortBubblesPerso(state.pages);
                        resolve({
                            pages: state.pages,
                            newPage: null,
                            carouselItems
                        });
                        AsyncStorage.setItem("todokDBNeedUpdate" + ref, "true");
                        AsyncStorage.setItem("todokDBNeedUpdate" + props.lists[props.selected].ref, "true");
                    } else {
                        resolve({ pages: state.pages, newPage: null, carouselItems: state.carouselItems });
                    }
                });
            }
        );
    }).then((value) => {
        res = value;
    });
    return res;
}

export function editPage(state, index, db) {
    let res = {};

    state.carouselItems[index].page = state.newPage;
    res = { ...res, carouselItems: state.carouselItems };
    AsyncStorage.setItem("todokDBNeedUpdate" + state.pages[state.activeIndex].ref, "true");
    db.transaction(
        tx => {
            tx.executeSql("update pages set name = (?) where id = (?)", [state.newPage, state.pages[state.activeIndex].ref]);
        },
        null,
    );
    return res;
}

export function removePage(time, state, props, page, db) {
    let res = {};
    let newItems = [], toDelete = [];
    let { carouselItems } = state;

    if (props.lists[props.selected].type === 'calendar') {
        for (let i in carouselItems) {
            if (carouselItems[i].page !== page) {
                newItems.push(carouselItems[i]);
            } else {
                toDelete.push(carouselItems[i]);
            }
        }
        res = { ...res, carouselItems: newItems, pageDelete: undefined, plusVisible: false };
        for (let i in toDelete)
            for (let j in toDelete[i].bubbles)
                removeBubbleFromList(time, state, props, toDelete[i].bubbles[j].ref, db);
    } else {
        let { pages } = state;
        let newPages = [], newId = 0;

        for (let i in pages) {
            if (i.toString() !== page.toString()) {
                newPages.push({ ...pages[i], id: newId });
                newId++;
            } else {
                toDelete.push({ ...carouselItems[i], index: i });
            }
        }
        for (let i in toDelete) {
            for (let j in toDelete[i].bubbles)
                removeBubbleFromList(time, state, props, toDelete[i].bubbles[j].ref, db);
            db.transaction(
                tx => {
                    tx.executeSql("delete from pages where id = (?)", [state.pages[toDelete[i].index].ref]);
                },
                null,
            );
        }
        let localPages = [];
        for (let i in pages)
            localPages.push(pages[i].ref)
        db.transaction(
            tx => {
                tx.executeSql("update lists set pages = (?) where id = (?)", [JSON.stringify(localPages), props.lists[props.selected].ref]);
            },
            null,
        );
        carouselItems = newSortBubblesPerso(newPages);
        res = { ...res, carouselItems, toDelete: undefined, pageDelete: undefined, plusVisible: false, pages: newPages };
    }
    return res;
}