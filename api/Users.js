import moment from 'moment';
import firebase from '../tools/firebase';
import { AsyncStorage, Alert } from 'react-native';

export async function getUserOnlineData(db, user, docRef) {
    await firebase.firestore().collection("users").doc(user.uid).update({ last_updated: moment().toISOString() }).catch((error) => {
        console.log(error);
    });
    let res = { archive: null, lists: null };
    db.transaction(
        tx => {
            tx.executeSql("drop table if exists users");
            tx.executeSql("drop table if exists lists");
            tx.executeSql(
                "create table if not exists users (archive BOOLEAN DEFAULT NULL, last_updated text, lists text, background text);"
            );
            tx.executeSql(
                "create table if not exists lists (id text, name text, type text, pages text, bubbles text);"
            );
            tx.executeSql(
                "create table if not exists bubbles (id text, archived text, date text, days text, name text, reminders text, repeat text, note text, tag text, underTask text, open text);"
            );
            tx.executeSql(
                "create table if not exists pages (id text, name text, bubbles text);"
            );
        },
        null,
    );
    db.transaction(
        tx => {
            tx.executeSql("insert into users (last_updated, lists) values (?, ?)", [new Date().toUTCString(), JSON.stringify([])]);
        },
        null,
    );
    if (docRef.exists) {
        if (docRef.data().archive) {
            archive = docRef.data().archive;
            db.transaction(
                tx => {
                    tx.executeSql("update users set archive = (?)", [docRef.data().archive]);
                },
                null,
            );
            res = { ...res, archive: docRef.data().archive };
        }
        if (docRef.data().lists) {
            let lists = [];
            let localLists = [];
            for (let i in docRef.data().lists) {
                let p = await firebase.firestore().collection("lists").doc(docRef.data().lists[i])
                    .get()
                    .then((data) => data)
                    .catch((error) => Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊'))
                lists.push({ id: i, name: p.data().name, type: p.data().type, ref: p.id });
                if (!!p.data().bubbles) {
                    db.transaction(
                        tx => {
                            tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [p.id, p.data().name, p.data().type, p.data().bubbles ? JSON.stringify(p.data().bubbles) : null]);
                        },
                        null,
                    );
                } else if (!!p.data().pages) {
                    db.transaction(
                        tx => {
                            tx.executeSql("insert into lists (id, name, type, pages) values (?, ?, ?, ?)", [p.id, p.data().name, p.data().type, p.data().pages ? JSON.stringify(p.data().pages) : null]);
                        },
                        null,
                    );
                }
                localLists.push(p.id);
            }
            db.transaction(
                tx => {
                    tx.executeSql("update users set lists = (?)", [JSON.stringify(localLists)]);
                },
                null,
            );
            res = { ...res, lists };
        } else {
            const id1 = Math.random().toString(36).substring(3);
            const id2 = Math.random().toString(36).substring(3);
            const bubbleRef = 'local' + user.uid + 'Todok' + id1 + id2;
            const id12 = Math.random().toString(36).substring(3);
            const id22 = Math.random().toString(36).substring(3);
            const bubbleRef2 = 'local' + user.uid + 'Todok' + id12 + id22;
            const id1L = Math.random().toString(36).substring(3);
            const id2L = Math.random().toString(36).substring(3);
            const listRef = 'local' + user.uid + 'Todok' + id1L + id2L;
            let lists = [];
            db.transaction(
                tx => {
                    tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                        bubbleRef,
                        "Merci d'être là et d'utiliser todok",
                        "Todok a été créé par deux étudiants français, todok vous accompagne dans chacune de vos tâches.",
                        JSON.stringify(["Autre"]),
                        JSON.stringify([
                            { id: 0, name: "Créer une tâche", archived: false },
                            { id: 1, name: "Archiver cette tâche", archived: false },
                        ]),
                        "false"
                    ]);
                    tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                        bubbleRef2,
                        "Pour m'archiver, clique une fois sur le rond. Pour me supprimer, maintiens le rond.",
                        "",
                        JSON.stringify([]),
                        JSON.stringify([]),
                        "false"
                    ]);
                    tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [listRef, "Bienvenue 😊🎊", "calendar", JSON.stringify([bubbleRef, bubbleRef2])]);
                    tx.executeSql("update users set lists = (?)", [JSON.stringify([listRef])]);
                }
            );
            await firebase.firestore().collection('bubbles').doc(bubbleRef).set({
                name: "Merci d'être là et d'utiliser todok",
                note: "Todok a été créé par deux étudiants français, todok vous accompagne dans chacune de vos tâches.",
                tag: ["Autre"],
                underTask: [
                    { id: 0, name: "Créer une tâche", archived: false },
                    { id: 1, name: "Archiver cette tâche", archived: false },
                ],
                open: "false",
            });
            await firebase.firestore().collection('bubbles').doc(bubbleRef2).set({
                name: "Pour m'archiver, clique une fois sur le rond. Pour me supprimer, maintiens le rond.",
                note: "",
                tag: [],
                underTask: [],
                open: "false",
            });
            await firebase.firestore().collection('lists').doc(listRef).set({ name: "Bienvenue 😊🎊", type: 'calendar', bubbles: [bubbleRef, bubbleRef2] });
            await firebase.firestore().collection('users').doc(user.uid).update({ lists: [listRef] }).catch(error => console.log(error));
            lists.push({ id: 0, name: "Bienvenue 😊🎊", type: "calendar", ref: listRef });
            res = { ...res, lists };
        }
    } else {
        const id1 = Math.random().toString(36).substring(3);
        const id2 = Math.random().toString(36).substring(3);
        const bubbleRef = 'local' + user.uid + 'Todok' + id1 + id2;
        const id12 = Math.random().toString(36).substring(3);
        const id22 = Math.random().toString(36).substring(3);
        const bubbleRef2 = 'local' + user.uid + 'Todok' + id12 + id22;
        const id1L = Math.random().toString(36).substring(3);
        const id2L = Math.random().toString(36).substring(3);
        const listRef = 'local' + user.uid + 'Todok' + id1L + id2L;
        let lists = [];
        db.transaction(
            tx => {
                tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                    bubbleRef,
                    "Merci d'être là et d'utiliser todok",
                    "Todok a été créé par deux étudiants français, todok vous accompagne dans chacune de vos tâches.",
                    JSON.stringify(["Autre"]),
                    JSON.stringify([
                        { id: 0, name: "Créer une tâche", archived: false },
                        { id: 1, name: "Archiver cette tâche", archived: false },
                    ]),
                    "false"
                ]);
                tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                    bubbleRef2,
                    "Pour m'archiver, clique une fois sur le rond. Pour me supprimer, maintiens le rond.",
                    "",
                    JSON.stringify([]),
                    JSON.stringify([]),
                    "false"
                ]);
                tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [listRef, "Bienvenue 😊🎊", "calendar", JSON.stringify([bubbleRef, bubbleRef2])]);
                tx.executeSql("update users set lists = (?)", [JSON.stringify([listRef])]);
            }
        );
        await firebase.firestore().collection('bubbles').doc(bubbleRef).set({
            name: "Merci d'être là et d'utiliser todok",
            note: "Todok a été créé par deux étudiants français, todok vous accompagne dans chacune de vos tâches.",
            tag: ["Autre"],
            underTask: [
                { id: 0, name: "Créer une tâche", archived: false },
                { id: 1, name: "Archiver cette tâche", archived: false },
            ],
            open: "false",
        });
        await firebase.firestore().collection('bubbles').doc(bubbleRef2).set({
            name: "Pour m'archiver, clique une fois sur le rond. Pour me supprimer, maintiens le rond.",
            note: "",
            tag: [],
            underTask: [],
            open: "false",
        });
        await firebase.firestore().collection('lists').doc(listRef).set({ name: "Bienvenue 😊🎊", type: 'calendar', bubbles: [bubbleRef, bubbleRef2] });
        await firebase.firestore().collection('users').doc(user.uid).update({ lists: [listRef] }).catch((err) => console.log(err));
        lists.push({ id: 0, name: "Bienvenue 😊🎊", type: "calendar", ref: listRef });
        res = { ...res, lists };
    }
    return res;
};

export async function getUserLocalData(db, hasInternet, user) {
    let res;

    await new Promise((resolve, reject) => {
        db.transaction(
            tx => {
                tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                tx.executeSql("select * from users", [], async (_, { rows }) => {
                    if (rows.length > 0) {
                        let archiveData = rows.item(0).archive == 0;
                        let localLists = JSON.parse(rows.item(0).lists);
                        let command = "";
                        command += "id = '" + localLists[0] + "'";
                        for (let i = 1; localLists[i]; i++)
                            if (localLists[i] != "")
                                command += " or id = '" + localLists[i] + "'";
                        let lists = [];
                        tx.executeSql("select * from lists where " + command, [], async (_, { rows }) => {
                            for (let i = 0; i != rows.length; i++) {
                                let p = rows.item(i);
                                lists.push({ id: i, name: p.name, type: p.type/*, bubbles: p.bubbles, pages: p.pages*/, ref: p.id });
                            }
                            if (await AsyncStorage.getItem('todokDBNeedUpdate') && hasInternet) {
                                await AsyncStorage.removeItem('todokDBNeedUpdate');
                                console.log("Need upload local data")
                                let onlineLists = [];
                                for (let i in lists)
                                    onlineLists.push(lists[i].ref)
                                firebase.firestore().collection("/users").doc(user.uid).update({ archive: archiveData, lists: onlineLists }).catch((error) => console.log(error));
                            }
                            resolve({ archive: archiveData, lists })
                        });
                    } else {
                        reject("No data for user.");
                    }
                });
            },
            null,
        );
    }).then((value) => {
        res = value;
    });
    return res;
}